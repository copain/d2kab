# Accès au sparql endpoint

Actuellement, le sparql endpoint est accessible sur [http://ontology.inrae.fr/bsv_test/sparql]().

Les URI des éléments sont en http://ontology.inrae.fr/bsv/…. **Il n'y a pas de déréférencement** des éléments de *bsv_test*.

Si le sparql endpoint est sur *bsv_test*, c'est en attente de validation de la structure des données. Ensuite, il est prévu de le mettre dans `http://ontology.inrae.fr/bsv/` qui contient actuellement les données du projet VESPA.


