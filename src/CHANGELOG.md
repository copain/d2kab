# Changelog

## Commit du 22/08/2022

Adaptation du plan xR2RML.

Fonctionne avec le plan AlvisNLP correspondant au commit
17746bebd19b7434cf83945d9a018eaaf1089168 du git termExtraction
(git@gitlab-ssh.irstea.fr:copain/d2kab.git), branche SB.

##### Plan xR2RML pour les annotations

Un première étape encore incomplète pour le remplacement du processus
d'intégration des annotations dans le triplestore.

À l'issue de ce commit :

- Seul fcu-baseline-annotations.csv a été testé (les autres devraient
  être identiques),
- les oa:ResourceSelection et oa:…Selector sont toujours des
  blank nodes, à l'instar du plan d'annotations de Nadia,
- le oa:XPathSelector a été supprimé, faute de pouvoir recomposer
  le XPath à partir des sorties d'AlvisNLP,
- le prov:Activity correspondant à l'exécution du plan d'annotations
  n'est plus généré (il faisait partie intégrante du script python
  d'intégration des annotations).

##### Modification des URI des BSV et de leurs réalisations.  

Précédemment,
ces URI étaient en ex:resources/REGION/ANNEE/BSV{,_pdf,_html}.

Dorénavant, la partie REGION/ANNEE/ est supprimée.

La raison est que REGION et ANNEE ne sont pas «connus» du processus
d'annotation. Celui-ci s'applique sur une liste de fichiers,
sans effectuer de requête dans le triplestore. Auparavant, c'était
le programme python d'intégration des annotations dans le triplestore
qui faisait le lien entre le nom du fichier annoté et sa réalisation
dans le triplestore.

Néanmoins, REGION/ANNEE/ a été conservé dans les liens de téléchargement
des réalisations pdf et html des BSV, pour éviter de devoir stocker
l'ensemble des BSV dans un seul et même répertoire, ce qui provoque
de forts ralentissements sur certains systèmes de fichiers (notamment
ext4).

## Commit du 11/08/2022

Ajout d'un fichier de configuration pour xR2RML.

La conversion des csv en ttl fonctionne, en utilisant le plan de Nadia
tel quel, mais les résultats ne correspondent pas au diagramme
des annotations.


## Commit du 10/08/2022

Premier commit après la création de la branche 1.0.

- Suppression du programme python pour intégrer les annotations.
- Ajout du plan xR2RML de Nadia Yacoubi.

Devrait fonctionner avec le commit 68c2f273b6cb4ed1446ff11f81dcb79b2f7e1230 
du git contenant le travail d'Anna (git@gitlab-ssh.irstea.fr:copain/d2kab.git),
mais pas encore testé.
