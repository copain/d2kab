# Répertoire src/

## etl

Code source python (v3) utiliser pour mettre "en ligne" (dans le triplestore)
les données sur les BSV. Dans un premier temps, effectue la mise en ligne
des BSV des corpus de test.

## collecte

Le "Draaf web crawler" utilisé pour la collecte des BSV.

Utilisation :

```sh
python -u collect.py | tee COLLECT.log
```
