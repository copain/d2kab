from bs4 import BeautifulSoup # https://www.crummy.com/software/BeautifulSoup/bs4/doc/
from pathlib import Path # Pour lire le contenu d'un fichier (lors du développement)
from urllib.parse import urljoin, urlparse # Pour composer les URLs relatives et valider.
#from urllib.request import urlopen #, urlretrieve # Pour le téléchargement
import urllib.request as ureq # Téléchargements.
from datetime import datetime

import unicodedata
import requests
import arrow
import time
import json
import ssl
import re

from config import VERBOSITY, ERROR_PREFIX
from config import VISITED, SEEN, REDIRECTED, INVALID_URL
from config import BASE_PREFIX, SPARQL_SERVER
from config import DEST_PATH, CERT_FILE

# To manage error 429:
PAUSE = 1
MAX_PAUSE = 10
LONG_SLEEP = 600
LAST_HTTP_GET = time.time()

# https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename
def slugifyterm(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    WARNING : Deletes dots
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    #value = re.sub(r'[^\w\s-]', '', value.lower())
    value = re.sub(r'[^\w\s-]', '', value)
    return re.sub(r'[-\s]+', '-', value).strip('-_')

def slugify(value, allow_unicode=False):
    res = ''
    value = str(value)
    for s in value.split('/'):
        if len(s) > 0:
            w = ''
            for d in s.split('.'):
                if len(d) > 0:
                    if len(w) == 0:
                        w = slugifyterm(d)
                    else:
                        w = "%s.%s" % (w,slugifyterm(d))
            if len(w) > 0:
                if len(res) == 0:
                    res = w
                else:
                    res = "%s/%s" % (res, w)
    #res = re.sub(r'([^\.])pdf$',r'\1.pdf', res)
    return res

# https://stackoverflow.com/questions/7160737/how-to-validate-a-url-in-python-malformed-or-not
def url_check(url):
    #min_attr = ('scheme' , 'netloc')
    try:
        result = urlparse(url)
        if all([result.scheme, result.netloc]):
            return True
        else:
            return False
    except:
        return False


def get_page_content(URL, path):
    global PAUSE, MAX_PAUSE, LONG_SLEEP, LAST_HTTP_GET

    if not url_check(URL):
        return INVALID_URL

    been_visited = True
    try :
        VISITED.index(URL)
    except :
        been_visited = False
    if (been_visited):
        return SEEN

    VISITED.append(URL)
    try :
        if not URL.startswith('http'):
            return Path(URL).read_text()
    except:
        return '%s (file access)' % (ERROR_PREFIX)
    err429 = True
    while err429:
        err429 = False
        now = time.time()
        if (now - LAST_HTTP_GET) > PAUSE:
            time.sleep(PAUSE) # Should be (now - LAST_HTTP_GET - PAUSE) but… be cool.
        LAST_HTTP_GET = time.time()
        try:
          r = requests.get(URL, verify=CERT_FILE)
        except:
            if VERBOSITY > 2 :
                print("Error requests.get(%s)" % URL)
                return '%s (requests.get)' % (ERROR_PREFIX)
        err429 = r.status_code == 429
        if err429:
            PAUSE += 1
            sleep = PAUSE
            if PAUSE > MAX_PAUSE:
              PAUSE = (MAX_PAUSE / 2)
              sleep = LONG_SLEEP
            if VERBOSITY > 3:
                print("%s %d : Sleeping %d seconds" % (ERROR_PREFIX, r.status_code, sleep))
            time.sleep(sleep)
    if (r.status_code != requests.codes.ok):
        return '%s %d' % (ERROR_PREFIX, r.status_code)
    for hi in r.history:
        try :
            loc = hi.headers['location']
        except :
            loc = 'loc should not end with .pdf here, right ?'
        #if hi.headers['location'].endswith('.pdf'):
        if loc.endswith('.pdf'):
            download(r.url, path.split('/')[-1], path)
            return '%s from %s\n(dbg : path = [%s])' % (REDIRECTED, URL, path)
    return r.text


def download(url, txt, loc):
    global PAUSE, MAX_PAUSE, LONG_SLEEP, LAST_HTTP_GET

    ### Has it been loaded already ?
    headers = {'Accept':'application/json'}
    query = {'query': """prefix dwl: <%s>
    SELECT ?dwl WHERE { <%s> a dwl:BSVDownload ; dwl:datetime ?dwl ;
         dwl:status "OK". }""" % (BASE_PREFIX, url)}
    req = requests.get(SPARQL_SERVER+'query', params=query, headers=headers)
    if (req.status_code != requests.codes.ok):
        return '%s %d' % (ERROR_PREFIX, req.status_code)
    s = json.loads(req.text)
    if (len(s['results']['bindings']) > 0):
        #if (VERBOSITY > 4):
        #    print("Already downloaded : <%s>" % s['results']['bindings'][0]['dwl']['value'])
        return "Already done (%s)" % s['results']['bindings'][0]['dwl']['value']

    ### New BSV available.
    ##### Create directories if doesn't exist.
    loc = slugify(loc, allow_unicode=True)
    path = re.split('/', loc)
    p = Path(DEST_PATH)
    for rep in path:
        p = p / rep
        try:
            if (not p.exists()): p.mkdir()
        except: return '%s (creating directory %s)' % (ERROR_PREFIX, p)
        if (not p.is_dir()): return '%s (cannot create directory %s)' % (ERROR_PREFIX, p)

    filename = loc+'/'+slugify(url.split('/')[-1])

    ##### Insert into triplestore
    query = { 'update': """prefix dwl: <%s>
    prefix xsd: <http://www.w3.org/2001/XMLSchema#>
    INSERT DATA { <%s> a dwl:BSVDownload ;
      dwl:local_file "%s";
      dwl:has_text "%s";
      dwl:datetime "%s"^^xsd:dateTime.
    }""" % (BASE_PREFIX, url, filename,
            txt, str(arrow.get(datetime.now()))) }

    req = requests.post(SPARQL_SERVER+'update', data=query)
    if (req.status_code != requests.codes.ok):
        return '%s %d' % (ERROR_PREFIX, req.status_code)

    ##### Download
    dest = DEST_PATH+'/'+filename
    if (VERBOSITY > 4):
        #print("Download : %s" % url)
        #print("      to : %s" % dest)
        print("  Download to : %s" % dest)

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    try:
        now = time.time()
        if (now - LAST_HTTP_GET) > PAUSE:
            time.sleep(PAUSE)
        LAST_HTTP_GET = time.time()
        ##urlretrieve(url, filename=dest, cafile=CERT_FILE)
        with ureq.urlopen(url, context=ctx) as src, open(dest,"wb") as dst:
            data = src.read()
            dst.write(data)
    except: return '%s (downloading %s into %s)' % (ERROR_PREFIX, url, dest)

    ##### Insert status OK into triplestore.
    query = { 'update': """prefix dwl: <%s>
    prefix xsd: <http://www.w3.org/2001/XMLSchema#>
    INSERT DATA { <%s> dwl:status "OK" }""" % (BASE_PREFIX, url) }
    req = requests.post(SPARQL_SERVER+'update',  data=query)
    if (req.status_code != requests.codes.ok):
        return '%s %d' % (ERROR_PREFIX, req.status_code)

    return 'OK'




def explore_draaf_01(location, url, exclude):
    return explore_draaf_01_r(location, url, '', '', exclude)


KNOWN_CLASSES = ['listeart', 'listerub', 'liens', 'texte']
def explore_draaf_01_r(path, url, link_url, recurse, exclude):
    global LAST_HTTP_GET, PAUSE

    if (urljoin(url, link_url).endswith('.pdf')):
      if (VERBOSITY > 4):
        # (Download) ?
        print("%s    [%s]" % (recurse, urljoin(url, link_url)))
      dwl = download(urljoin(url, link_url), path, path)
      if (dwl.startswith(ERROR_PREFIX)):
        if (VERBOSITY > 2):
          print('%s%s' % (recurse, dwl))
      return

    if exclude is not None:
      for rexp in exclude:
          if rexp.match(path):
              if (VERBOSITY > 3): print('%s xx %s xx' % (recurse, path))
              return

    if (VERBOSITY > 3): print('%s--> %s' % (recurse, path))
    time.sleep(PAUSE)
    html_doc = get_page_content(urljoin(url, link_url), path)
    LAST_HTTP_GET = time.time()
    #try:
    #  html_doc = get_page_content(urljoin(url, link_url), path)
    #except:
    #  if (VERBOSITY > 1):
    #      print("Error in get_page_content")
    #      html_doc = ERROR_PREFIX
    if (html_doc.startswith(ERROR_PREFIX)) or (html_doc.startswith(INVALID_URL)):
        if (VERBOSITY > 2): print('%s%s : [%s]' % (recurse, html_doc, urljoin(url, link_url)))
        return
    if (html_doc.startswith(REDIRECTED)):
        if (VERBOSITY > 2): print('%s%s' % (recurse, html_doc))
        return
    if (html_doc == SEEN):
        if (VERBOSITY > 2): print('%s[%s] already seen.' % (recurse, urljoin(url, link_url)))
        return

    try:
      soup = BeautifulSoup(html_doc, 'html.parser')
    except:
      if (VERBOSITY > 1):
          print("Error in BeautifulSoup : [%s])" % (urljoin(url, link_url)))

    found = False
    for li in soup.find_all('a'):
        if (((li.has_attr('href') and (li['href'].endswith('.pdf')))
            or (li.has_attr('class') and (li['class'] == 'titreartliste')))
            and not (li.has_attr('rel') and (li['rel'] == 'nofollow external'))):
            found = True
            txt = li.string
            if (VERBOSITY > 4):
                # (Download) ?
                print("%s    [%s]" % (recurse, urljoin(url, li['href'])))
            dwl = download(urljoin(url, li['href']), txt, path)
            if (dwl.startswith(ERROR_PREFIX)):
                if (VERBOSITY > 2):
                    print('%s%s' % (recurse, dwl))
    #if (found): return

    for cl in KNOWN_CLASSES:
        for art in soup.find_all(attrs={'class': cl}):
            #if art.a is not None:
            for a in art.find_all('a'):
                if ((VERBOSITY > 4) and not found): print('%s[ %s ]'%(recurse,cl))
                found = True
                # explore_draaf_01_r(path+'/'+art.a.string, urljoin(url, link_url),
                #                     art.a['href'], '%s  '%recurse)
                if (a.string is not None):
                  explore_draaf_01_r(path+'/'+re.sub('\/','-',a.string),
                                     urljoin(url, link_url),
                                     a['href'], '%s  '%recurse, exclude)
