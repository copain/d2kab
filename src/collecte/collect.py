from config import SOURCES, VERBOSITY
#from wrappers import draaf_main_01
from wrappers import explore_draaf_01

from pathlib import Path # Pour lire le contenu d'un fichier lors du développement
from config import TEST

import re

#  Exécution :
#  python -u collect.py | tee COLLECT.log
#
#  Traitement des erreurs :
#  grep ERROR COLLECT.log
#
#  Nombre de BSVs téléchargeables :
#  grep \(Download COLLECT.log | wc -l
#
#  Nombre de BSVs déjà téléchargés :
#  grep ^Already COLLECT.log.old | wc -l
#
#  … pour avoir le nombre de BSVs téléchargés :
#  grep "to : " COLLECT.log | wc -l

# To easily disable some (won't collect if not in this list) :
collect_regions = [
  "ARA",
  "BFC",
  "Bretagne",
  "CVL",
  "Corse",
  "GrandEst",
  "HdF",
  "IdF",
  "Normandie",
  "NvelleAquitaine",
  "Occitanie",
  "PACA",
  "PdL",
  "Gwa",
  "May",
]

#
# Remarque : Lancer avec "python -u" pour qu'il n'y ait pas de cache sur stdout
#
for s in SOURCES:
  if (VERBOSITY > 1): print("----> %s\n" % s['location'])
  if s['location'] in collect_regions:

    if (not TEST):
        exclude = s.get('exclude')
        if exclude is not None:
            regexp = [re.compile(e) for e in exclude]
        else:
            regexp = None
        explore_draaf_01(s['location'], s['URL'], regexp)

    else:
        ## Tests :
        ## Start fuseki server with :
        ## cd ~/opt/share/apache-jena-fuseki-3.8.0
        ## ./fuseki-server --update --mem /bsv_dwnl
        #explore_draaf_01(s['location'], 'samples/ARA/toto.html')
        #explore_draaf_01(s['location'], 'samples/BFC/BFC.html')
        explore_draaf_01(s['location'], 'samples/Bretagne/Bretagne.html')
