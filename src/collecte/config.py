
VERBOSITY = 5

TEST = False

# Where to download BSVs. Directory must exist.
DEST_PATH = '/home/stef/Boulot/Data/BSV/BSV-Collecte'

CERT_FILE = '/home/stef/Boulot/Devel/collecte/cert-DRAAF.pem'

# TODO: Fill this comment
SOURCES = [
  { 'location': "ARA",
    'URL': 'https://draaf.auvergne-rhone-alpes.agriculture.gouv.fr/Bulletin-de-sante-du-vegetal',
    'exclude' : [
		r'ARA/Archives BSV',
	],
  },
  { 'location': "BFC",
    'URL': 'https://draaf.bourgogne-franche-comte.agriculture.gouv.fr/BSV-Bourgogne-Franche-Comte,32',
    ###'URL': 'http://draaf.bourgogne-franche-comte.agriculture.gouv.fr/Bulletins-de-Sante-du-Vegetal-BSV' },
    'exclude': [
      r'BFC/20(16|17|18|19)',
      r'BFC/2017-2018',
      r'BFC/2018-2019',
      r'BFC/2019-2020',
    ],
  },
  { 'location': "Bretagne",
    'URL': 'https://draaf.bretagne.agriculture.gouv.fr/Bulletins-de-Sante-du-Vegetal-BSV',
    'exclude': [
      r'Bretagne/Archives', r'Bretagne/.*20(14|15|16|17|18|19|20)$',
    ],
  },
  { 'location': "CVL",
    'URL': 'https://draaf.centre-val-de-loire.agriculture.gouv.fr/Acces-au-BSV',
    'exclude': [
      r'CVL/Archives BSV',
    ],
  },
  { 'location': "Corse",
    'URL': 'https://draaf.corse.agriculture.gouv.fr/Les-bulletins-de-sante-du-vegetal',
    'exclude': [
      r'Corse/BSV 20(14|15|16|17|18|19|20)',
      r'Corse/[^/]+/BSV 20(14|15|16|17|18|19|20)',
      r'Corse/.*20(14|15|16|17|18|19|20)$',
    ],
  },
  { 'location': "GrandEst",
    'URL': 'https://draaf.grand-est.agriculture.gouv.fr/Surveillance-des-organismes',
    'exclude': [
      r'GrandEst/Bulletins de santé du végétal 2020',
      r'GrandEst/Bulletins de santé du végétal - 201(6|8|9)',
      r'GrandEst/Bulletins de santé du végétal 2017',
      r'GrandEst/Bulletin de Santé du Végétal - Archives 2015, 2016',
      r'GrandEst/.* 2015$',
    ],
  },
  { 'location': "HdF",
    'URL': 'https://draaf.hauts-de-france.agriculture.gouv.fr/Les-Bulletins-de-sante-du-vegetal',
    'exclude': [
      r'HdF/20(16|17|18|19|20)',
      r'HdF/[^/]+/20(16|17|18|19|20)',
     ],
  },
  { 'location': "IdF",
    'URL': 'https://driaaf.ile-de-france.agriculture.gouv.fr/BSV-annee-en-cours',
  },
  { 'location': "Normandie",
    'URL': 'https://draaf.normandie.agriculture.gouv.fr/Bulletin-de-sante-du-vegetal',
    'exclude': [
      r'Normandie/[^/]+/Parutions 20(15|16|17|18|19|20)',
      r'Normandie/Campagne 20(15|16|17|18|19|20)',
      r'Normandie/[^/]+/Campagne 20(15|16|17|18|19|20)',
    ],
  },
  { 'location': "NvelleAquitaine",
    'URL': 'https://draaf.nouvelle-aquitaine.agriculture.gouv.fr/Bulletin-de-sante-du-vegetal',
    'exclude': [
      r'NvelleAquitaine/BSV Nouvelle-Aquitaine 20(16|17|18|19)',
     ],
  },
  { 'location': "Occitanie",
    'URL': 'https://draaf.occitanie.agriculture.gouv.fr/Bulletins-de-sante-du-vegetal',
    'exclude': [
      r'Occitanie/20(16|17|18|19)',
      r'Occitanie/[^/]+/20(16|17|18|19)',
     ],
  },
  { 'location': "PACA",
    'URL': 'https://draaf.paca.agriculture.gouv.fr/Bulletin-de-Sante-du-Vegetal-BSV',
    'exclude': [
      r'PACA/Année 20(15|16|17|18|19|20)',
      r'PACA/[^/]+/Année 20(15|16|17|18|19|20)',
      r'PACA/20(15|16|17|18|19)',
      r'PACA/[^/]+/20(15|16|17|18|19)',
      r'PACA/Archives',
      r'PACA/[^/]+/Archives',
     ],
  },
  { 'location': "PdL",
    'URL': 'https://draaf.pays-de-la-loire.agriculture.gouv.fr/Derniers-BSV' },
  { 'location': "Gwa",
    'URL': 'http://daaf.guadeloupe.agriculture.gouv.fr/Bulletin-de-Sante-du-Vegetal',
    'exclude': [
      r'Gwa/BSV 20(14|15|16|17|18|19|20)',
      r'Gwa/[^/]/BSV 20(14|15|16|17|18|19|20)',
      r'Gwa/BSV bilans annuels/BSV bilan 20(14|15|16|17|18|19|20)',
      r'Gwa/Archives BSV',
    ],
  },
  { 'location': "May",
    'URL': 'https://daaf.mayotte.agriculture.gouv.fr/Bulletin-de-Sante-du-Vegetal-BSV' },
]

# TODO: Comment this
ERROR_PREFIX = '>>> ERROR'
SEEN = '>>> ALREADY VISITED'
REDIRECTED = '>>> REDIRECTED'
INVALID_URL = '>>> INVALID URL'

if (TEST):
    SPARQL_SERVER = 'http://localhost:3030/bsv_dwnl/'
else:
    SPARQL_SERVER = 'http://ontology.irstea.fr:3030/bsv_dwnl/'

BASE_PREFIX = 'http://ontology.irstea.fr/bsv_dwnl/'

# Below is not configuration anymore, but just global variable declarations.
VISITED = []
