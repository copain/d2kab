# ETL

Contient les éléments permettant l'intégration des données BSV
dans le triplestore.

## Structure

- ttl/ : Les fichiers ttl pour les éléments statiques. Tous les préfixes
  sont à mettre dans prefixes.ttl, pas de préfixe dans les autres fichiers :
  ces fichiers ttl seront découpés en clauses "INSERT DATA"
- py/ : Les codes python nécessaires à l'intégration des données BSV.
- getIndex/ : La précédente version de stockage. Les trop nombreux changements
  au diagramme ne permettent pas de la réutiliser telle quelle mais ses
  éléments seront petit à petit intégré à etl/.
- BSV.drawio : contient la version des diagrammes qui correspond à ce qui
  est intégré dans le triplestore.

## Alimentation du triplestore

Dans un premier temps, seuls les corpus de test sont
intégrés dans le triplestore.

```sh
cd ~/Boulot/Devel/d2kab/src/etl/py

# Éditer config.py et ajuster les valeurs, notamment celles du triplestore.

## Ceci est nécessaire pour créer l'activité d'organisation
## On peut ne pas le faire à condition de ne pas avoir touché au code
## depuis le dernier commit.
#### Obsolete depuis le 6/04/2022 :
## L'activité d'organisation ne désigne plus que Jena-Fuseki.
## Mais faire un commit préalable à l'exécution reste une bonne pratique.
#git commit -m "Version exécutée le $(date +'%d/%m/%Y')."

cat ../ttl/{vespa,agents,collections,activities}.ttl | python -u doSparqlUpdate.py

# Ajout de l'activité d'organisation.
## Important : éditer ../sh/update_jena-fuseki.sh pour vérifier le lien
## de téléchargement de la version installée de jena-fuseki.
../sh/update_jena-fuseki.sh -f

export DEST_PATH=/home/stef/Boulot/Data/BSV/files
export DEST_PATH=None # Si on ne veut pas copier les fichiers

# Ajout du Corpus de test VESPA:
ls ~/Boulot/Data/BSV/Corpus/corpus-vespa/bsv/pdf/* | 
  python -u vespa2ttl.py --copy_bsv=$DEST_PATH --corpus_test | 
  python -u doSparqlUpdate.py

# Ajout du corpus de test Alea:
find ~/Boulot/Data/BSV/Corpus/corpus-alea/bsv/pdf/ -type f | 
  python -u d2kab2ttl.py --copy_bsv=$DEST_PATH --corpus_test=Alea | 
  python doSparqlUpdate.py

# Ajout du corpus de test D2KAB:
find ~/Boulot/Data/BSV/Corpus/corpus-d2kab/bsv/pdf/ -type f | 
  python -u d2kab2ttl.py --copy_bsv=$DEST_PATH --corpus_test=D2KAB |
  python doSparqlUpdate.py

# Conversion en html
## RQ : AVANT conversion, vérifier que la librairie marche bien
##      (à cause d'une MÀJ de poppler par exemple, et comme les erreurs
##       sont silencieuses - la plupart étant "malformed pdf" - il
##       se trouve que l'on ne s'en rend pas compte).
##
## python
## import camelot
## dir = '/home/stef/Boulot/Data/BSV/BSV-download/HdF/Arboriculture-fruitiere/2022/'
## doc = dir+'BSV-Arboriculture-fruitiere-n10-du-26-avril-2022/BSV_AF_n10_du_26042022_cle0ce9e7.pdf'
## tl = camelot.read_pdf(doc, backend="poppler", pages="all") ### LOOOOONG
## print(tl[0].df) ### Doit donner le tableau de la page 2.
##
## Si ça ne marche pas :
##   pikaur -S python-camelot
##   pikaur -S python-opencv
##   pikaur -S python-pdftopng
## …ne pas oublier de "nettoyer avant compilation" (à l'invite).
## Si tout marche :

python -u convertToHtml.py --dest_path=$DEST_PATH --force_sparql --proc=15 2>errors.txt| 
  python doSparqlUpdate.py

### ATTENTION ### Lors de la conversion html, si $DEST_PATH est None,
                # alors on n'aura pas le nombre de mots dans le fichier.

### ANNOTATIONS ### Actuellement (pour les corpus de test), il faut
                # une copie des BSVs en html corpus par corpus.
                # Il est plus simple de refaire tourner le pdf2blocks.
                # Voir ~/Boulot/Data/BSV/Corpus/README.txt (en bas)
                # Ensuite :
  # cd '/home/stef/Boulot/Devel/Anna/termExtraction/d2kab'
  # ~/opt/share/alvisnlp/bin/alvisnlp main-annotation.plan


# Et enfin, ajout des annotations.
## RQ: La version sans xR2RML est dans la branche v1.0 du git

# IMPORTANT: Actuellement l'ajout des annotations n'est pas fonctionnel.
#  Pour une version fonctionnelle, voir la branche v1.0

cd ../xR2RML
export CSVDIR=~/Boulot/Devel/Anna/termExtraction/d2kab/output/annotations

## Pour effacer la base mongodb :
# mongosh
# use d2kab
# db.Entities.drop()
# [Ctrl-d]

cat $CSVDIR/fcu-baseline-annotations.csv | tr '\t' ',' |
  mongoimport --db=d2kab -c Entities --type=csv --ignoreBlanks --headerline --drop

java -jar /home/stef/opt/share/xr2rml/bin/morph*.jar \
  --configDir $(pwd) --configFile annotation.conf \
  --mappingFile annotation.ttl


```

### Copie des tailles de fichiers html

On a fait les conversions des pdf en html localement (parce que c'est
long) et ça n'est pas la peine de le refaire pour mettre en ligne.

Donc on a choisi `export DEST_PATH=None` pour effectuer la mise
en ligne. Du coup, on n'a pas le *frac:total* qui indique le nombre 
de mots (et qui est calculé par pdf2blocks).

Et donc on va faire une requête UPDATE avec une clause SERVICE.
On l'exécute en local :

```bash
##### ATTENTION : Il faut corriger l'IP de ma machine (10.63.65.16)
#####             dans la clause SERVICE

s-update --service=http://ontology.irstea.fr:3030/bsv_test/update """
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX dce: <http://purl.org/dc/elements/1.1/>
INSERT { GRAPH <http://ontology.inrae.fr/bsv/graph/bsv> {
  ?bsv <http://www.w3.org/ns/lemon/frac#total> ?nb_car }}
WHERE {
 ?bsv a dct:Text ;
   dce:format \"text/html\" .
  SERVICE <http://10.63.65.16:3030/bsv/query> {
    GRAPH <http://ontology.inrae.fr/bsv/graph/bsv> {
      ?bsv <http://www.w3.org/ns/lemon/frac#total> ?nb_car
    }
  }
}"""
```

La requête pour vérifier :
```sparql
SELECT DISTINCT ?resource ?fractotal ?value
WHERE { ?resource <http://www.w3.org/ns/lemon/frac#total> ?value ; ?fractotal ?value }
```

## Effacement du contenu d'un dataset BSV

```sh
#export endpoint=http://ontology.inrae.fr:3030/bsv_test
export endpoint=http://localhost:3030/bsv
s-update --service=${endpoint}/update "DROP GRAPH <http://ontology.inrae.fr/bsv/graph/ontology>"
s-update --service=${endpoint}/update "DROP GRAPH <http://ontology.inrae.fr/bsv/graph/agents>"
s-update --service=${endpoint}/update "DROP GRAPH <http://ontology.inrae.fr/bsv/graph/corpus>"
s-update --service=${endpoint}/update "DROP GRAPH <http://ontology.inrae.fr/bsv/graph/activities>"
s-update --service=${endpoint}/update "DROP GRAPH <http://ontology.inrae.fr/bsv/graph/bsv>"
s-update --service=${endpoint}/update "DROP GRAPH <http://ontology.inrae.fr/bsv/graph/annotations>"
```

## Démarrage d'un Jena-Fuseki local

```sh
~/opt/share/jena-fuseki/fuseki-server --config=/home/stef/Boulot/Srv/fuseki/CONFIG.ttl

## Et pour avoir le snorql qui va avec :
cd ~/Boulot/Srv/snorql
webfsd -f index.html -F -p 8000 # Et après on va sur le port 8000
```

## Mise à jour de jena-fuseki

En plus de la procédure de mise à jour sur le serveur,
il faut mettre à jour l'activité d'organisation.

Pour cela, il suffit d'éditer le script update_jena-fuseki.sh
en donnant le lien de téléchargement. Il faut aussi s'assurer
que le FUSEKI_HOME pointe bien sur la version en cours d'exécution
(le script appelle $FUSEKI_HOME/useki-server --version),
puis il suffit d'exécuter le script.
