### Some tool functions
import unicodedata
import re

## --- To make "valid" filenames

# https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename
def slugifyterm(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    WARNING : Deletes dots
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value)
    return re.sub(r'[-\s]+', '-', value).strip('-_')[:124]

def slugify(value, allow_unicode=False):
    res = ''
    value = str(value)
    for s in value.split('/'):
        if len(s) > 0:
            w = ''
            for d in s.split('.'):
                if len(d) > 0:
                    if len(w) == 0:
                        w = slugifyterm(d)
                    else:
                        w = "%s.%s" % (w,slugifyterm(d))
            if len(w) > 0:
                if len(res) == 0:
                    res = w
                else:
                    res = "%s/%s" % (res, w)
    return res
