# Algorithme d'indexation des BSVs

## Collecte des BSV
Nous avons commencé à collecter nos premiers BSV en 2012 dans le cadre
du projet VESPA (2012 - 2016). Dans ce projet, nous avons mis en place
un processus de collecte semi automatique. Les bulletins de santé du végétal
étaient récupérés depuis les sites web des chambres d'agriculture de leur
région, page par page.
À noter qu'avant le premier janvier 2016, il y avait 27 régions.
Cette collecte était faite environ deux fois par an.

Dans chaque région, on trouve en général une page web pour chaque type
de culture, dans laquelle figure la liste des bulletins téléchargeables.
Avec parfois des liens vers des archives classées par année.
Un outil tel que [DownThemAll!](https://www.downthemall.org) permet en un clic
de télécharger l'ensemble des bulletins de la page et de les enregistrer
dans un dossier sur le disque dur local. Nous avons donc créé une structure
de répertoires sous la forme `Région/Année/Type de culture` pour stocker
les BSV téléchargés.

En 2018, un script a été écrit pour automatiser la collecte des BSV
sur les sites des chambres d'agriculture. Les informations relatives
au téléchargement de nouveaux bulletins font l'objet d'une entrée
dans un jeu de données. On y stocke :

- le lien de téléchargement du BSV,
- la date de téléchargement,
- le nom du fichier pdf du BSV téléchargé,
- la chaîne de caractères de l'hyperlien pointant sur le fichier pdf du bulletin.

Les sites web des chambres d'agriculture ayant un système de gestion de contenu
(CMS) commun, ce script fonctionne pour toutes les régions,
à l'exception de la Réunion, la Martinique et la Guyane qui utilisent
un autre CMS.

Nous avons donc, à l'heure actuelle, deux ensembles de bulletins :

- les BSV indexés lors du projet VESPA,
- les BSV qui ont été collectés automatiquement, qui ne sont pas indexés mais
  qui sont associés à des informations collectées lors du téléchargement.

Nous avons focalisé l'indexation sur la dernière catégorie, pour aller vers
une chaîne de collecte et d'indexation de plus en plus automatisée. Tout
en visant à être le plus générique possible pour pouvoir fonctionner avec
le moins d'adaptations possibles avec l'autre groupe de bulletins. Toutefois,
les BSV du projet VESPA ayant déjà été indexés, il n'est pas nécessaire
de les ré-indexer, sauf dans le cas où les résultats seraient meilleurs.

Actuellement, 35825 BSV ont été téléchargés.

- 19448 BSV ont été indexés dans le projet Vespa
- 16377 BSV ont été téléchargés automatiquement.

Il faut garder à l'esprit que les processus de téléchargement automatiques,
que ce soit avec DownThemAll! ou avec un script, ne sont pas exempts
d'erreurs. En particulier, lorsque ce qui est renvoyé n'est pas le fichier
attendu, ces processus ne sont pas toujours capables de détecter cette erreur.

Ainsi, 46 fichiers ne sont pas reconnus comme étant des fichiers pdf par
la commande file (28 indexés dans VESPA, 18 téléchargés automatiquement).

## Évaluation

Pour évaluer les méthodes d'extraction de données, on dispose de plusieurs
ensembles de BSV :

- corpus BSV:  35825 BSV ont été téléchargés. Ce corpus se décompose
  en un corpus vespa et un corpus auto.
  - corpus vespa: les 19448 BSV indexés dans le projet VESPA
    et qui sont donc datés avant 2016.  
    - corpus de test vespa: un sous ensemble du corpus vespa composé de 500 BSV
      dont les index ont été construits manuellement. Ce corpus de test
      a été utilisé pour vérifier la qualité de processus d'indexation
      automatique utilisé pour indexer l'ensemble du corpus vespa.
  - corpus auto: 16377 BSV ont été collectés automatiquement depuis 2017
    jusqu'à maintenant (novembre 2020).
    - corpus de test d2kab: un corpus de 230 bulletins sur la vigne,
      le maraîchage (tomate, salade, carotte) et les grandes cultures,
      mis en place dans le cadre du projet D2KAB, disponible sur
      [le gitlab](https://gitlab.irstea.fr/copain/d2kab/-/tree/master/corpus_test).
      Ce corpus de test est indexé manuellement pour vérifier la qualité
      des processus de TALN mis en place lors du projet d2kab.
    - corpus de test alea: un sous-ensemble de 150 bulletins tirés aléatoirement
      parmi les bulletins collectés automatiquement. Ce corpus de test
      est utilisé pour effectuer une  seconde série de test des processus
      d'indexation du projet d2kab.


### Corpus de test VESPA

Le développement de l'indexation dans le cadre du projet Vespa a donné lieu
à une extraction manuelle de date pour 500 bulletins de santé du végétal.

Les régions et types de cultures ont été manuellement extraits lors
des collectes de ces BSV.

On dispose donc d'un corpus permettant de tester l'extraction automatique
de dates. Ces bulletins ayant été collectés avant 2016, les zones géographiques
correspondent au découpage régional avant la réforme du 1er janvier 2016.

En ce qui concerne les types de culture, il s'agit de rubriques indiquées dans
les pages web de téléchargement (Grandes cultures, Viticulture, Jardins, ...).
Chaque rubrique a été associée manuellement à un ensemble de concepts
issu du thésaurus french crop usage. À noter que les rubriques varient
d'une région à l'autre et qu'il n'y a pas de liste harmonisée
des types de cultures. C'est pour cette raison que nous avons créé le thésaurus
french crop usage.

### Corpus de test D2KAB

Dans le cadre du projet D2KAB, un corpus de 230 BSV a été constitué. Ce corpus
visant à extraire des informations sur la base de reconnaissance de vocabulaire,
il porte sur des bulletins traitant de maraîchage, de viticulture et de grandes
cultures uniquement.

Il ne contient que des BSV (il ne contient pas de notes nationales sur un
ravageur, une adventice, ...). Ceci fait que tous les bulletins qu'il contient
ont une date de publication précise.

### Corpus de test aléa

Le corpus de test D2KAB étant relativement homogène, l'extension du processus
d'indexation à l'ensemble des BSV collectés automatiquement nécessite
un corpus plus aléatoire. 150 autres BSV ont donc été tirés aléatoirement
parmi l'ensemble des BSV collectés automatiquement jusqu'à ce jour
(novembre 2020).

De par son caractère aléatoire, 8 documents n'ont pas de date précise,
10 ont une zone géographique inter-régionale voire nationale, et 1 bulletin
n'a pas de type de culture mentionné dans son contenu.


## Indexation
### Date du bulletin
#### L'algorithme de *DateParser*

La détection de dates en français a été développée dans une classe python
nommée *DateParser*. Le principe est le suivant : instanciée avec une liste
d'expressions régulières, cette classe offre des méthodes effectuant
la recherche de ces expressions régulières dans des chaînes de caractère
et retourne les dates rencontrées au format *datetime.date*.

Le seul aspect spécifiquement lié au français dans cette classe est
d'enrichir les capacités de reconnaissance des abréviations ou d'absence
d'accents dans l'écriture des mois de la fonction *date.strptime*.
Elle peut donc facilement être adaptée à d'autres langues.

*DateParser* nécessite un certain nombre de paramètres :

- une liste d'expressions régulières décrivant les formats de date,
- une date de début et de fin : en dehors de ces dates, toute date rencontrée
  est rejetée. Ceci permet d'écarter des dates "absurdes", et, dans la mesure
  où l'on est certain de ne pas avoir de date entre 2001 et 2013, de ne pas
  avoir de confusion entre mois et année. Par défaut, les dates recherchées
  sont comprises entre *[l' epoch Unix](https://fr.wikipedia.org/wiki/Heure_Unix)*
  et la veille du jour courant.
- Une localisation (nécessaire pour une utilisation adaptée de la librairie
  datetime). Par défaut, les dates sont en français et utilisent l'UTF-8.


#### Recherche de la date d'un BSV

Comme dans le projet VESPA, la recherche d'une date pour un bulletin de santé
du végétal s'appuie sur plusieurs méthodes.

##### Recherche dans le nom de fichier
Il arrive souvent que le nom du fichier contienne la date de publication du BSV.
Quand c'est le cas, cette date est généralement fiable. Les erreurs viennent
plutôt de ce qu'une suite de chiffres soit par erreur identifié comme une date.
En cela, l'intervalle de dates valides de *DateParser* permet de prévenir
cette erreur.

Il y a 20960 fichiers dans le corpus BSV dans le nom desquels est reconnue
une date :
- 8357 dans le corpus Vespa (43% des fichiers du corpus),
- 12603 dans le corpus auto (77% des fichiers constituant le corpus).

Dans la suite de ce document, nous considérerons une "date exemple" pour décrire
les formats de date, afin d'éviter les écritures du type "jj mm aaaa",
ou "j MM aa", ...
En prenant l'exemple du 3 septembre 2020, les formats de date recherchés dans
le nom de fichier sont :

- 3 Septembre 2020, 03 septembre 2020, 3 sep 2020, 03 sept 2020, ...
- 3 septembre 20, 03 septembre 20, 3 sep 20, 03 sep 20, ...
- 03-09-2020, 3_9_2020, 3.09.2020
  (caractère de séparation quelconque, non alphanumérique)
- 2020-09-03, 2020.9.3, ... (même remarque que précédemment)
- 03/09/20, 3/9/20, 3/09/20, ... (avec obligatoirement un '/' à la française)
- 20200903
- 03092020
- 03-09-20, 03.09.20, (séparateur quelconque)

À noter que dans les expressions régulières, le '\W', qui signifie "tout
caractère non alphanumérique", ne comprend pas l'underscore '_'.
Celui-ci étant couramment utilisé comme séparateur dans les noms de fichiers,
un traitement préalable est effectué pour les remplacer par des tirets '-'
afin qu'ils soient reconnus.

Les formats numériques "année, mois, jour", s'ils ne correspondent pas
aux usages de la langue française, sont couramment utilisés en informatique
pour que l'ordre alphabétique corresponde à l'ordre chronologique. C'est
pourquoi ils ont été intégrés aux formats recherchés.

La **date retenue** est la première date reconnue en parcourant cette liste
de formats, **dans l'ordre** où ils sont donnés ici.

Si aucune date n'est trouvée dans le nom du fichier, on recherche une date
dans la hiérarchie des répertoires. En effet, certains sites créent une page
web pour chaque bulletin. Le processus de collecte reproduit cette structure
en créant la hiérarchie des répertoires correspondant, et nous avons parfois
des structures de répertoires telles que :

    /HdF/Cultures légumières/2020/BSV Cultures légumières n°18 du 6 août 2020/BSV_Legumes_182020_cle8dfd68.pdf

Si aucune date n'a été reconnue à ce stade, on teste, pour le nom de fichier
uniquement, les dates sous la forme 200903.
On ne recherche sous cette forme que des dates après 2012 (pour ne pas confondre
mois et année) à condition que l'intervalle de dates le permette.

##### Recherche dans les données de collecte

Lors de la collecte automatique des BSV, la chaîne de caractères de l'hyperlien
vers le bulletin de santé du végétal à télécharger est stockée
dans un jeu de données. Sur certains sites, cette chaîne de caractères comprend
la date de publication du bulletin. On trouve par exemple :
*"Bulletin de santé du végétal n°1 du 29-09-2019"* ou
*"BSV Céréales N°3-26 du 21 octobre 2015"*.

On ne cherchera pas dans cette chaîne de caractères les mêmes formats
de date que dans les noms de fichiers. On cherchera avant tout des dates
écrites en langue naturelle.

Les formats de dates recherchés sont alors (en reprenant l'exemple
du 3 septembre 2020) :

- 3 Septembre 2020, 03 septembre 2020, 3 sep 2020, 03 sept 2020, ...
- 3 septembre 20, 03 septembre 20, 3 sep 20, 03 sep 20, ...
- 03/09/20, 3/9/20, 3/09/20, ... (avec obligatoirement un '/' à la française)
- 03-09-2020, 3_9_2020, 3.09.2020
  (caractère de séparation quelconque, non alphanumérique, y compris le '/)

Comme pour le nom de fichier, la **date retenue** est la première date reconnue
en parcourant cette liste de formats, dans l'ordre où ils sont donnés ici.

##### Recherche dans le contenu du BSV

Il s'agit de chercher la date de publication d'un BSV à partir de l'extraction
du contenu textuel du BSV, obtenu avec l'outil
[pdf2blocs](https://gitlab.irstea.fr/copain/pdf2blocs).

Dans le texte, on recherche les mêmes formats de dates que dans le texte
de l'hyperlien, à savoir les façons courantes d'écrire une date dans un document
en français. On ne recherche pas de date dans un format réduit ou inversé
comme dans les noms de fichier.

La méthode consiste à relever toute date trouvée dans le texte du bulletin.

Deux dates sont retenues :

- la première date rencontrée,
- la date ayant le plus grand nombre d’occurrences.

Les résultats sont meilleurs avec la première date rencontrée. Toutefois,
la date ayant le plus grand nombre d’occurrences sera utilisée dans le processus
de sélection d'une date.


#### Évaluation comparative des trois méthodes

Notre algorithme recherche uniquement une date précise (jour, mois, année). Or
tous les bulletins de santé du végétal n'ont pas de date de publication précise.
C'est le cas pour des bilans annuels par exemple, mais aussi des notes
nationales, qui n'ont pas une date complète (jour, mois, année). La plupart
du temps ces bulletins sont associés à une année, mais pas toujours.

Les BSV des corpus de test pour lesquels aucune date de publication n'a été
identifiée par une lecture humaine sont désignés comme étant des BSV
"non datés".

Les tableaux suivants utilisent l'abbréviation « Nb » au lieu de l'expression
« Nombre de » pour des raisons de mise en page.

##### Les corpus de test

| Corpus de test | Nb BSV | Nb BSV datés | Nb BSV non datés |
|----------------|-------:|-------------:|-----------------:|
| Vespa          | 500 | 472 | 28 |
| D2kab          | 230 | 230 |  0 |
| Alea           | 150 | 142 |  8 |
| ***Total***    | ***880*** | ***844*** _(96%)_ | ***36*** _(4%)_ |


##### Nombre de dates trouvées

| _Méthode de recherche :_      | Vespa | D2kab | Aléa | Total |
|:------------------------------|------:|------:|-----:|------:|
| Dans le nom de fichier        |   249 |   191 |   92 |   532 |
| Dans le texte de l'hyperlien  |       |   103 |   60 |   163 |
| Dans le contenu du BSV        |   462 |   230 |  147 |   829 |

###### Remarques
La méthode de recherche dans le texte de l'hyperlien n'a pas pu être appliquée
au corpus de test Vespa, car le texte des hyperliens n'a pas été conservé
lors de la collecte semi-automatique effectuée dans le cadre de ce projet.

##### Nombre de dates exactes

| _Méthode de recherche :_                 | Vespa      | D2kab      | Aléa       | Total       |
|:-----------------------------------------|-----------:|-----------:|-----------:|------------:|
| Dans le nom de fichier                   | 226 _(91%)_| 179 _(94%)_|  90 _(98%)_| 495 _(93%)_ |
| Dans le texte de l'hyperlien             |            |  82 _(80%)_|  56 _(93%)_| 138 _(85%)_ |
| Dans le BSV : **1ère date trouvée**      | 423 _(92%)_| 225 _(98%)_| 139 _(95%)_| 787 _(95%)_ |
| Dans le BSV : **date la plus fréquente** | 397 _(86%)_| 214 _(93%)_| 128 _(87%)_| 739 _(89%)_ |

###### Remarques

On dit que la date retournée par le processus est exacte lorsque cette date
est identique à la date de publication relevée manuellement.

On ne prend pas en considération le cas où aucune date n'a été trouvée
dans un bulletin non-daté, malgré que ce cas puisse être considéré comme étant
positif. Pour information, lors de la recherche dans le contenu du BSV,
le nombre de bulletins non datés pour lesquels la recherche n'a donné aucune
date est de :

- 15 pour le corpus de test Vespa,
-  3 dans le corpus de test Aléa,
- aucune dans le corpus de test D2kab (corpus ayant tous les bulletins datés).

Si l'on tient compte de ceci, l'extraction de la première date trouvée
dans le contenu du BSV a un taux de réussite de 97%.

Dans le tableau, les pourcentages sont donnés par rapport au nombre de dates
trouvées, ce qui permet d'évaluer la fiabilité de la méthode
_lorsqu'elle donne un résultat_, et donc indépendamment de sa capacité
à donner un résultat. On peut l'interpréter ainsi : « Lorsque le processus
trouve une date dans le nom du fichier d'un BSV d'un corpus de test, celle-ci
est exacte dans 93% des cas. »

Les valeurs des taux sont arrondies à l'entier le plus proche.

##### Nombre de dates proches (écart ≤ 7 jours)

Considérant arbitrairement qu'un écart maximum de 7 jours avec la date exacte
est une erreur acceptable, on a ajouté aux dates exactes les dates trouvées
ayant moins d'une semaine d'écart avec la date de publication du BSV.
Ceci donne le tableau suivant :

| _Méthode de recherche :_                 | Vespa      | D2kab      | Aléa       | Total       |
|:-----------------------------------------|-----------:|-----------:|-----------:|------------:|
| Dans le nom de fichier                   | 240 _(96%)_| 188 _(98%)_|  91 _(99%)_| 519 _(98%)_ |
| Dans le texte de l'hyperlien             |            |  99 _(96%)_| 60 _(100%)_| 159 _(98%)_ |
| Dans le BSV : **1ère date trouvée**      | 436 _(94%)_| 228 _(99%)_| 139 _(95%)_| 803 _(97%)_ |
| Dans le BSV : **date la plus fréquente** | 419 _(91%)_| 226 _(98%)_| 135 _(92%)_| 780 _(94%)_ |


#### Sélection d'une date, par combinaison des trois méthodes

Sur les trois méthodes décrites, on considère 4 dates :

1. la première date rencontrée dans le contenu du BSV,
1. la date du nom de fichier,
1. la date du lien hypertexte lors de la collecte,
1. la date la plus souvent rencontrée dans le contenu du BSV.

Ces quatre dates sont données selon un rang d'efficacité suite aux tests
qui ont été effectués. Il s'agit du classement correspondant au taux
d'exactitude des dates trouvées. Nous avons toutefois remonté la recherche dans
le texte de l'hyperlien avant la date ayant le plus d'occurences dans le BSV ;
en effet, cette méthode, si elle donne moins de résultats exacts, donne
des résultats beaucoup plus proches de la date exacte.

La date qui sera considérée comme date de publication du bulletin,
sera déterminée ainsi :

- Si aucune date n'est retournée par aucun de ces processus,
  il sera considéré que le BSV n'a pas de date de publication.
  On pourrait imaginer rechercher une année, mais ceci n'a pour le moment
  pas été considéré.
- Si une seule date a été trouvée, alors c'est celle-ci qui sera retenue.
- Si deux dates sont trouvées, celle du processus jugé le plus efficace
  sera retenue.
- Au delà de deux dates, une condition supplémentaire est ajoutée, visant à
  éviter qu'une méthode retournant une date trop éloignée des autres
  ne soit retenue :
  - pour chaque date trouvée, on calcule la somme du nombre de semaines
    d'écart avec chacune des autres dates,
  - on retient la somme minimum,
  - la date retenue est celle renvoyée par la méthode jugée la plus efficace
    parmi celles dont l'écart aux autres dates est minimum.

##### Évaluation de la méthode de détermination de la date de publication d'un BSV

En appliquant la méthode de sélection décrite ci-dessus, nous obtenons
les résultats suivants (quelques explications sont nécessaires et sont données
dans les remarques qui suivent le tableau) :

| _Méthode de recherche :_                 | Vespa      | D2kab      | Aléa       | Total       |
|:-----------------------------------------|-----------:|-----------:|-----------:|------------:|
| _Nombre de BSV_                          |        500 |        230 |        150 |         880 |
| _Nombre de BSV datés_                    |        472 |        230 |        142 |         844 |
| Nombre de dates trouvées                 |        476 |        230 |        147 |         853 |
| Nombre de dates exactes                  | 442 _(93%)_| 228 _(99%)_| 140 _(95%)_| 810 _(95%)_ |
| Nombre de réponses exactes               | 456 _(91%)_| 228 _(99%)_| 143 _(95%)_| 827 _(94%)_ |
| Nombre de dates proches                  | 453 _(95%)_|230 _(100%)_| 140 _(95%)_| 823 _(94%)_ |
| Nombre de réponses proches               | 467 _(93%)_|230 _(100%)_| 143 _(95%)_| 840 _(95%)_ |

###### Remarques

Le taux de dates exactes est donné relativement au nombre de dates trouvées,
de même que le taux de dates proches. Le nombre de dates trouvées ayant
augmenté, ce taux paraît moins bon que les taux donnés méthode par méthode.
Cependant, le nombre de dates exactes a augmenté.

Les **réponses exactes** incluent, en plus des dates exactes, les BSV non datés
pour lesquels le processus n'a pas renvoyé de date. Le taux correspondant
est donné relativement au nombre de BSV dans le corpus. C'est pour cela qu'il
semble moins bon que celui des "dates exactes" alors que le nombre a augmenté.
Toutefois ce taux correspond à une évaluation de la méthode dans son ensemble,
et peut être interprété comme « le processus a renvoyé 94% de réponses exactes
à la détermination de la date des 880 BSV des corpus de test».

De façon similaire, les **réponses proches** sont les dates proches auxquelles
ont été ajoutés les bulletins non datés pour lesquels le processus n'a pas
renvoyé de date.


### Annotation des BSV

L'annotation des BSV est faite par
[AlvisNLP](https://github.com/Bibliome/alvisnlp) en utilisant
[les plans réalisés par Anna Chepaikina](https://github.com/kouklinaa/termExtraction).
Ce processus génère un dichier csv contenant les résultats.

L'intégration dans le triplestore est réalisée à l'aide du programme
`addAnnotations.py` qui utilise ce fichier csv.

Le fichier contient les colonnes suivantes :

- "bsv" :
  Le fichier **et le chemin** contenant le texte du BSV au format html
  (sortie de pdf2blocks). Le chemin est important : les BSV sont classés
  par une structure de répertoires et sous-répertoires générée par getIndex,
  qui définit l'URI des BSV.
- "context before" :
  Quelques caractères avant l'occurence.
- "form" :
  Le mot tel que dans le texte.
- "lemma" :
  Le mot normalisé pour la recherche.
- "prefLabel" :
  Le `skos:prefLabel` du concept trouvé.
- "context after" :
  Quelques caractères après l'occurence.
- "location" :
  La position de l'occurence dans le texte.
- "features" :
  Un champ particulier, sous la forme `clé=valeur` séparés par des virgules.
  En particulier, on utilise la valeur de la clé "uri", qui contient l'URI
  du concept de *frenchcropusage* correspondant à l'occurence en question.

La première ligne du fichier doit contenir les noms de colonnes, tels
que listés ici, sans les guillemets. L'ordre n'a pas d'importance.
Un caractère de tabulation est utilisé pour séparer les colonnes du
fichier csv.

L'ajout se fait directement dans le triplestore à l'aide de commandes UPDATE.
Les annotations se font dans le graphe 
`<http://ontology.inrae.fr/bsv/annotations>`.


### Diagrammes
#### Les BSV

![BSV](BSV.svg)

#### Les annotations

![Annotations](Annotation.svg)









