import re
from sys import stdin
from datetime import datetime, date, timedelta, timezone
import locale

###################
# CONSTANT VALUES #
###################
MONTH_SUB = [ # Substitutions. strptime n'acceptant, pour %b, que :
              # janv. févr. mars avril mai juin juil. août sept. oct. nov. dec,
              # alors qu'on a des "fev", des "aout", …
  {'regex' : r'jan[^v]', 'sub' : 'janvier'},
  {'regex' : r'f[ée]v[^r]', 'sub' : 'février'},
  {'regex' : r'fevrier', 'sub' : 'février'},
  {'regex' : r'mar[^r]', 'sub' : 'mars'},
  {'regex' : r'avr[^i]', 'sub' : 'avril'},
  {'regex' : r'juil[^l]', 'sub' : 'juillet'},
  {'regex' : r'ao[uû][^t]', 'sub' : 'août'},
  {'regex' : r'aout', 'sub' : 'août'},
  {'regex' : r'sep[^t]', 'sub' : 'septembre'},
  {'regex' : r'sept[^e]', 'sub' : 'septembre'},
  {'regex' : r'oct[^o]', 'sub' : 'octobre'},
  {'regex' : r'nov[^e]', 'sub' : 'novembre'},
  {'regex' : r'd[eé]c[^e]', 'sub' : 'décembre'},
  {'regex' : r'decembre', 'sub' : 'décembre'},
]

MONTH_RE = r"""(?P<month>(jan(?:vier)? | f[eé]v(?:rier)? | mar(?:s)?
 | avr(?:il)? | mai | juin | juil(?:let)? | ao[ûu](?:t)? | sep(?:t(?:embre)?)?
 | oct(?:obre)? | nov(?:embre)? | d[eé]c(?:embre)?))"""


#
#
#
class DateParser:
    "Un outil pour chercher des dates (en français) dans des chaînes de caractères."

    #####################
    # STATIC ATTRIBUTES #
    #####################
    MONTH_SUB = [ # Substitutions. strptime n'acceptant, pour %b, que :
              # janv. févr. mars avril mai juin juil. août sept. oct. nov. dec,
              # alors qu'on a des "fev", des "aout", …
              {'regex' : r'jan[^v]', 'sub' : 'janvier'},
              {'regex' : r'f[ée]v[^r]', 'sub' : 'février'},
              {'regex' : r'fevrier', 'sub' : 'février'},
              {'regex' : r'mar[^r]', 'sub' : 'mars'},
              {'regex' : r'avr[^i]', 'sub' : 'avril'},
              {'regex' : r'juil[^l]', 'sub' : 'juillet'},
              {'regex' : r'ao[uû][^t]', 'sub' : 'août'},
              {'regex' : r'aout', 'sub' : 'août'},
              {'regex' : r'sep[^t]', 'sub' : 'septembre'},
              {'regex' : r'sept[^e]', 'sub' : 'septembre'},
              {'regex' : r'oct[^o]', 'sub' : 'octobre'},
              {'regex' : r'nov[^e]', 'sub' : 'novembre'},
              {'regex' : r'd[eé]c[^e]', 'sub' : 'décembre'},
              {'regex' : r'decembre', 'sub' : 'décembre'},
    ]

    MONTH_RE = r"""(?P<month>(jan(?:vier)? | f[eé]v(?:rier)? | mar(?:s)?
 | avr(?:il)? | mai | juin | juil(?:let)? | ao[ûu](?:t)? | sep(?:t(?:embre)?)?
 | oct(?:obre)? | nov(?:embre)? | d[eé]c(?:embre)?))"""

    FMT_DAY = 0
    FMT_MONTH = 1
    FMT_YEAR = 2

    GET_FIRST = 0x0001
    GET_MOST = 0x0002
    GET_ALL = 0x0004

    def __init__(self,
            date_formats = [{'regex': r'(?P<day>\d\d?)\W(?P<month>\d\d?)\W(?P<year>\d{4})', 'fmt':('%d','%m','%Y')}],
            loc = 'fr_FR.UTF-8', month_substitution = MONTH_SUB,
            from_date = date(1970,1,1),
            to_date = date.today() - timedelta(days = 1)):
        locale.setlocale(locale.LC_TIME, loc) # this is for datetime.strftime
        self._locale = loc
        self._date_formats = date_formats
        for f in self._date_formats:
            f['comp'] = re.compile(f['regex'], re.VERBOSE | re.IGNORECASE)
        self._month_substitution = month_substitution
        for f in self._month_substitution:
            f['comp'] = re.compile(f['regex'], re.VERBOSE | re.IGNORECASE)
        self._from = from_date
        self._to = to_date
        self._debug = 0

    @property
    def locale(self):
        return self._locale

    @locale.setter
    def locale(self, loc):
        locale.setlocale(locale.LC_TIME, loc)
        self._locale = loc

    @property
    def date_formats(self):
        return self._date_formats
    @date_formats.setter
    def date_formats(self, df):
        """date_formats is a list of dictionaries containing :
- 'regex': a regular expression (for re) of a date to be found in text.
  The regex should have a (?P<day>…), a (?P<month>…) and a (?P<year>…) sections.
  It will be compiled using re.VERBOSE and re.IGNORECASE options.
- 'fmt': a list, where the 1st element is the day format recognized by regex
  (used by datetime.strftime(…), for example '%d'), the second element is
  the month format (ex: '%B' or '%m') and the 3rd element is the year format."""
        self._date_formats = df
        for f in self._date_formats:
            f['comp'] = re.compile(f['regex'], re.VERBOSE | re.IGNORECASE)

    @property
    def month_substitution(self):
        return self._month_substitution
    @month_substitution.setter
    def month_substitution(self, ms):
        """A list of substitutions applied to month group when a date_format
element matches some text. For exemple, in french, february is sometimes written
«fev», or «fév», or «fevrier», …. But datetime.strptime only accepts «févr.»
with %b format and «février» with %B format (the lack of the accent isn't
recognized).
Then, by appling substitutions defined in month_substitution, we'll ensure that
all those different ways to write a month will be transformed in a way making
datetime.strptime work. For example, to have a correct form for février, we'll
put {'regex' : r'f[ée]v[^r]', 'sub' : 'février'} and
{'regex' : r'fevrier', 'sub' : 'février'} in month_substitution list.
The regex contained in this list are compiled using the flags re.VERBOSE
and re.IGNORECASE"""
        self._month_substitution = ms
        for f in self._month_substitution:
            f['comp'] = re.compile(f['regex'], re.VERBOSE | re.IGNORECASE)


    @property
    def from_date(self):
        return self._from

    @from_date.setter
    def from_date(self, from_date):
        """A date found before 'from' will be ignored."""
        self._from = from_date

    @property
    def to_date(self):
        return self._to

    @to_date.setter
    def to_date(self, to_date):
        """A date found after 'to' (default yesterday) will be ignored."""
        self._to = to_date

    @property
    def debug(self):
        return self._debug

    @debug.setter
    def debug(self, debg):
        """Verbosity level of the process (from 0 — silent & default — to 5)"""
        self._debug = debg


    def getDates(self, text, mode = GET_ALL, res = {}):
        """
The main function of this class.
It has been defined as a class because initialization of date_formats and
month_substitution requires compilation of regular expressions, and those
lists can be quite long to write. So, the aim was to instanciate DateParsers
once, then call gatDates as many times as needed.

**BE CAREFUL** Depending on the mode, the result can be different :
- it returns a datetime.date object for modes GET_MOST and GET_FIRST
- it returns a dictionary when using mode GET_ALL.
- it returns None if no date have been found.

The GET_FIRST mode is the fastest as it returns the first date found (matching
one of date_formats regular expression and after — or same as — from_date value
and before — or same as — to_date value).

The GET_MOST mode returns the date having the most occurence. In case of
equality, we use the order of date_formats (the first is best) to choose
the date returned (and if there is still an equality, the first encountered
date is returned).

The GET_ALL mode returns a dictionary, having dates as keys, and a list
of integers. At index 0 of this list is the number of occurences of this
date, then, at index 1 is the number of matches of the first date_formats
regular expression, at index 2 the number of matches of the second
date_formats regular expression, and so on.

The res parameter takes the result of a previous call in GET_ALL mode,
and the newly found dates are appended into it. This is to be able
to search dates in a long document, line per line or paragraph per
paragraph.
        """
        # We'll remove all double spaces, -_/* and dots.
        # Maybee this should be a parameter…
        p = re.compile(r"([\ \-_/*\.])\1{1,}",re.DOTALL)
        txt = p.sub(r"\1", ' '+text+' ')
        if self._debug > 0: print("===> [%s]" % txt)
        form_range = 0
        for form in self._date_formats:
            form_range += 1
            iter = form['comp'].finditer(txt)
            for match in iter:
                # Month replacements :
                mth = match.group('month')
                for msub in self._month_substitution:
                    if msub['comp'].search(mth) is not None:
                        mth = msub['comp'].sub(msub['sub'], mth)
                # Now we try to instanciate a datetime with values found.
                # If we obtain an exception, it's not a valid datetime.
                dtstr = "%s/%s/%s" % (match.group('day'), mth, match.group('year'))
                dtfmt = "%s/%s/%s" % (form['fmt'][self.FMT_DAY],
                                form['fmt'][self.FMT_MONTH],
                                form['fmt'][self.FMT_YEAR])
                if self._debug > 4: print("  ·> %s → %s" % (dtstr, dtfmt))
                try:
                    dt = datetime.strptime(dtstr, dtfmt).date()
                    if self._debug > 3:
                        print("           <--> %s" % dt.strftime('%d/%m/%Y'))
                    if (dt >= self.from_date) and (dt < self.to_date):
                        if mode == self.GET_FIRST:
                            return dt
                        if res.get(dt) is None:
                            res[dt] = [0 for i in range(1+len(self._date_formats))]
                            res[dt][0] = 1
                            res[dt][form_range] = 1
                        else:
                            res[dt][0] += 1
                            res[dt][form_range] += 1
                except ValueError:
                    pass
        if self._debug > 2:
            if self._debug > 3:
                print("")
                for k in  res.keys():
                    print("      ———> %s : %s" % (k, res[k]))
                if len(res.keys()) > 0 :
                    print("")
        if len(res) == 0 and mode != self.GET_ALL:
            return None
        if mode == self.GET_ALL:
            return res
        return self.getMost(res) # mode == GET_MOST





    def getMost(self, res):
        """
getDate(mode=GET_MOST) is the same as doing a getMost(getDate(mode=GET_ALL)) :
it returns the date having the most occurences. In case of equality,
we use the order of date_formats (the first is best) to choose
the date returned (and if there is still an equality, the first encountered
date is returned).
        """
        if len(res) == 0:
            return None
        occurences = [res[k][0] for k in res.keys()]
        oc_max = max(occurences)
        return_date = None
        first_matcher = 1+len(self._date_formats)
        first_matcher_value = 0
        for k in res.keys():
            if res[k][0] == oc_max:
                if (sum(res[k][1:first_matcher])) > 0:
                    best_fm = 1
                    while res[k][best_fm] == 0: best_fm += 1
                    if (best_fm < first_matcher) or res[k][best_fm] > first_matcher_value:
                        return_date = k
                        first_matcher = best_fm
                        first_matcher_value = res[k][best_fm]
        return return_date
