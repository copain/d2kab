import re
import requests
import arrow
import json

IGNORE_CROP_NAMES = [ 'soleil', 'd’hiver', 'marron', 'orange',
'gel', 'fruit', 'fleur', # 'horticulture', 'pépinière',
'côte', 'semence',
]

# In text, substitute the 'regex' matches with the 'sub' value
# before searching a crop name.
CROP_SUBSTITUTIONS = [
    {'regex': r'@orange\.fr', 'sub': '@orangefr'},
]

# In crop search regular expressions, instead of using the label (if found
# here), uses the regular expression.
REGEX_SUBSTITUTIONS = {
  'olivier': 'oliviers',
  # 'fleur': 'horticoles',
}

FCU_PREFIX = 'http://ontology.inrae.fr/frenchcropusage/'

class CropTree:

    def __init__(self,
      CROP_SPARQL_SERVER = 'http://ontology.irstea.fr/frenchcropusage/',
      CROP_PREFIX = FCU_PREFIX, DEBUG = 0 ):
        self._sparql_srv = CROP_SPARQL_SERVER
        self._prefix = CROP_PREFIX
        self._debug = DEBUG
        self._tree = {}

        query = {'query': """SELECT ?croptop ?crop WHERE {
        ?croptop <http://www.w3.org/2004/02/skos/core#narrower> ?crop }"""}
        # Get crop tree, using skos broaders
        hdrs = {'Accept':'application/json'}
        req = requests.get(self._sparql_srv+'query', params=query, headers=hdrs)
        if (req.status_code != requests.codes.ok):
            raise RuntimeError('SPARQL Error looking for skos:broader.')
            return
        else:
            s = json.loads(req.text)

        tree = self._tree
        for t in s['results']['bindings']:
            croptop = t['croptop']['value'][len(CROP_PREFIX):]
            crop = t['crop']['value'][len(CROP_PREFIX):]
            if tree.get(croptop) is None:
                tree[croptop] = {'broader':[], 'ambiguity': False, 'main': False, 'deep':1}
            if tree.get(crop) is None:
                tree[crop] = {'broader':[], 'ambiguity': False, 'main': False, 'deep':1}
            if croptop not in tree[crop]['broader']:
                tree[crop]['broader'].append(croptop)

        # Compute deepness
        chg = True
        while chg:
            chg = False
            for k in tree.keys():
                if k == 'FrenchCropUsage':
                    tree[k]['deep'] = 0
                else:
                  if len(tree[k]['broader']) > 0:
                    mx = max([tree[b]['deep'] for b in tree[k]['broader']]) + 1
                    if tree[k]['deep'] != mx:
                        chg = True
                        tree[k]['deep'] = mx

        # Check ambiguity
        query = {'query': """SELECT DISTINCT ?crop WHERE {
<%sPlantes_cultures_ambigues> <http://www.w3.org/2004/02/skos/core#member> ?crop}
             """ % CROP_PREFIX}
        req = requests.get(self._sparql_srv+'query', params=query, headers=hdrs)
        if (req.status_code != requests.codes.ok):
            raise RuntimeError('SPARQL Error looking for cultures_ambigues.')
            return
        else:
            s = json.loads(req.text)

        for t in s['results']['bindings']:
            crop = t['crop']['value'][len(CROP_PREFIX):]
            if tree.get(crop) is not None:
                tree[crop]['ambiguity'] = True

        def getUnambigusSons(crop_tree, key):
            ret = []
            for k in crop_tree.keys():
                if key in crop_tree[k]['broader']:
                    if crop_tree[k]['ambiguity']:
                        ret += getUnambigusSons(crop_tree, k)
                    else:
                        if k not in ret:
                            ret.append(k)
            return ret

        for k in tree:
            if tree[k]['ambiguity']:
                tree[k]['unambigus_sons'] = getUnambigusSons(tree, k)
            nup = 0 # nup = Number of Unambiguious Parents
            for p in tree[k]['broader']:
                if tree[p]['ambiguity']:
                    nup += 1
            tree[k]['nb_ambiguious_parents'] = max(nup, 1)

        # Get main crop categories
        query = {'query': """SELECT DISTINCT ?crop WHERE {
<%sGrandes_categories> <http://www.w3.org/2004/02/skos/core#member> ?crop}
             """ % CROP_PREFIX}
        req = requests.get(self._sparql_srv+'query', params=query, headers=hdrs)
        if (req.status_code != requests.codes.ok):
            raise RuntimeError('SPARQL Error looking for grandes_categories.')
            return
        else:
            s = json.loads(req.text)

        self._main_crop = {}
        for t in s['results']['bindings']:
            crop = t['crop']['value'][len(CROP_PREFIX):]
            if tree.get(crop) is not None:
                tree[crop]['main'] = True
                if self._main_crop.get(crop) is None: # Should always be True
                    self._main_crop[crop] = {'sons':[]}

        # Get sons of main crops
        # Remark : The main crop itself is contained in its son's list.
        #          OK, being its own son is somehow strange. Another name for
        #          son's list ? Descendance ? Same problem...
        def getSons(crop_tree, key):
            ret = []
            for k in crop_tree.keys():
                if key in crop_tree[k]['broader']:
                    ret += getSons(crop_tree, k)
            ret.append(key)
            return ret
        for m in self._main_crop.keys():
            self._main_crop[m]['sons'] = getSons(tree, m)

        # Get labels
        query = {'query': """SELECT ?crop ?label WHERE {
    {?crop  <http://www.w3.org/2004/02/skos/core#prefLabel> ?label}
    UNION {?crop <http://www.w3.org/2004/02/skos/core#altLabel> ?label}}
        """}
        req = requests.get(self._sparql_srv+'query', params=query, headers=hdrs)
        if (req.status_code != requests.codes.ok):
            raise RuntimeError('SPARQL Error while retreiving labels.')
            return
        else:
            s = json.loads(req.text)

        # {'label': ['crop_keys',]}
        self._labels = {}
        for t in s['results']['bindings']:
            if t['label']['value'].lower() not in IGNORE_CROP_NAMES:
                lbl = t['label']['value'].lower()
                lbl = re.sub(r"\(.*\)","", lbl)
                lbl = re.sub(r"[,\.]","",lbl)
                lbl = re.sub(r"[_-]"," ", lbl)
                lbl = re.sub(r"['\/]",".", lbl)
                lbl = lbl.strip()
                crop = t['crop']['value'][len(CROP_PREFIX):]
                if tree.get(crop) is not None:
                    if self._labels.get(lbl) is None:
                        self._labels[lbl] = [crop]
                    else:
                        if crop not in self._labels[lbl]:
                            self._labels[lbl].append(crop)

        def unaccent_fr(ch):
            ch = re.sub('[éèêë]','e',ch)
            ch = re.sub('[àâä]','a',ch)
            ch = re.sub('[ôö]','o',ch)
            ch = re.sub('[îï]','i',ch)
            ch = re.sub('[ùüû]','u',ch)
            return re.sub('ç','c',ch)

        # Create 'regex' for each crop
        lblk = [""+k for k in self._labels.keys()]
        lblk.sort(reverse=True, key=len)
        self._labels_list = lblk
        self._labels_comp = {}
        self._file_labels_comp = {}
        for lbl in lblk:
            if lbl in REGEX_SUBSTITUTIONS.keys():
                label = REGEX_SUBSTITUTIONS[lbl]
            else:
                label = lbl
            self._labels_comp[lbl] = re.compile(r"^%s\W|\W%s\W|\W%s$|^%s$" % (label,
                                        label, label, label), flags = re.IGNORECASE)
            self._file_labels_comp[lbl] = re.compile("\W%s\W" % unaccent_fr(label),
                                        flags = re.IGNORECASE)
            if lbl in REGEX_SUBSTITUTIONS.keys():
                label = REGEX_SUBSTITUTIONS[lbl]
            else:
                label = lbl
            for c in self._labels[lbl]:
                if tree[c].get('regex') is None:
                    tree[c]['regex'] = label
                else:
                    tree[c]['regex'] = r"%s|%s" % (tree[c]['regex'], label)
                for m in self._main_crop:
                    if c in self._main_crop[m]['sons']:
                        if self._main_crop[m].get('regex') is None :
                            self._main_crop[m]['regex'] = label
                        else:
                            self._main_crop[m]['regex'] = r"%s|%s" % \
                                (self._main_crop[m]['regex'], label)
        for k in tree.keys():
            if tree[k].get('regex') is not None:
                tree[k]['regex'] = r"\W(%s)\W" % tree[k]['regex']
                tree[k]['comp'] = re.compile(tree[k]['regex'], re.IGNORECASE)
        for m in self._main_crop:
            if self._main_crop[m].get('regex') is not None:
                self._main_crop[m]['regex'] = r"\W(%s)\W" % self._main_crop[m]['regex']
                self._main_crop[m]['comp'] = re.compile(self._main_crop[m]['regex'], re.IGNORECASE)



    @property
    def tree(self):
        return self._tree

    @property
    def debug(self):
        return self._debug

    @debug.setter
    def debug(self, debg):
        """Verbosity level of the process (from 0 — silent & default — to 5)"""
        self._debug = debg

    def __get_from_pages(self, crop_dict, pages, block_classes):
        res = {}
        for p in pages.values():
            for b in p['blocks']:
                if b['class'] in block_classes:
                    txt = b['text']
                    for su in CROP_SUBSTITUTIONS:
                        txt = re.sub(su['regex'], su['sub'], txt)
                    t = " %s " % txt
                    for k in crop_dict.keys():
                        if crop_dict[k].get('regex') is not None:
                            ma = crop_dict[k]['comp'].findall(t) # ma -> Matches
                            if len(ma) > 0:
                                if res.get(k) is None:
                                    res[k] = len(ma)
                                else:
                                    res[k] += len(ma)
        return res

    def get_main_crop_from_pages(self, pages, block_classes):
            return self.__get_from_pages(self._main_crop, pages, block_classes)
    def get_crop_from_pages(self, pages, block_classes):
            return self.__get_from_pages(self._tree, pages, block_classes)


    # ---- On teste ça...

    # Very usefull
    def zero_if_none(self, val):
        if val is None:
            return 0
        return val


    def get_labels_from_text(self, txt, file = True):
        res = {}
        if file:
            search_list = self._file_labels_comp
        else:
            search_list = self._labels_comp
        for lbl in self._labels_list:
            ma = search_list[lbl].findall(txt)
            if len(ma) > 0:
                #print("- %s - %s" % (lbl, txt))
                txt = search_list[lbl].sub('', txt)
                for k in self._labels[lbl]:
                    res[k] = self.zero_if_none(res.get(k)) + len(ma)
        return res

    # Correspond au précédent get_labels_from_pages(), qui renvoyait
    # en réalité les concepts, avec une pondération sur les ambiguités.
    def get_concepts_from_pages(self, pages, block_classes, verbosity = 0):
        res = {}
        prnt = False
        for p in pages.values():
            for b in p['blocks']:
                if b['class'] in block_classes:
                    txt = b['text']
                    for lbl in self._labels_list:
                        ma = self._labels_comp[lbl].findall(txt)
                        pos = 0
                        if len(ma) > 0:

                            # --- Do the treatment to print it
                            if verbosity > 4:
                                match = self._labels_comp[lbl].search(txt)
                                pos = 0
                                while match:
                                    if match.start() == 0:
                                        leftpos = pos
                                    else:
                                        leftpos = pos + match.start() + 1
                                    if pos+match.end() == len(txt):
                                        rightpos = pos + match.end()
                                    else:
                                        rightpos = pos + match.end() - 1
                                    if pos == 0:
                                        print(" - %s «%s[%s]%s»" % (lbl,
                                            txt[max(match.start()-30, 0):leftpos],
                                            txt[leftpos:rightpos],
                                            txt[rightpos:match.end()+30]))
                                    else:
                                        print("       «%s[%s]%s»" % (
                                            txt[max(match.start()-30+pos, 0):leftpos],
                                            txt[leftpos:rightpos],
                                            txt[rightpos:pos+match.end()+30]))
                                    pos += match.end()
                                    match = self._labels_comp[lbl].search(txt[pos:])
                                prnt = True
                                out = ''
                                for k in self._labels[lbl]:
                                    if self._tree[k]['ambiguity']:
                                        out2 = "{%s [A] ->" % k
                                        first = True
                                        for s in self._tree[k]['unambigus_sons']:
                                            if first:
                                                out2 = "%s %s" % (out2, s)
                                            else:
                                                out2 = "%s, %s" % (out2, s)
                                        out = "%s %s}" % (out, out2)
                                    else:
                                        out = "%s {%s}" % (out, k)
                                print("   └─>%s" % out)
                                # -- End print if verbosity > 4

                            txt = self._labels_comp[lbl].sub('', txt)
                            for k in self._labels[lbl]:
                                if self._tree[k]['ambiguity']:
                                    l = len(self._tree[k]['unambigus_sons'])
                                    for s in self._tree[k]['unambigus_sons']:
                                        res[s] = self.zero_if_none(res.get(s)) \
                                                + float(len(ma) / l)
                                else:
                                    res[k] = self.zero_if_none(res.get(k)) + len(ma)
        if prnt:
            print("")
        return res

    # Renvoie les chaînes trouvées avec le nombre d'occurences.
    # Si block_classes est None, fait la recherche quelle que soit
    # la classe du bloc (équivalent à [0..<plus_haut_bloc>]
    #
    # V2.0 : Renvoie un dico de concepts, chacun étant un dico de termes
    # avec le nombre d'occurences.
    def get_labels_from_pages(self, pages, block_classes, verbosity = 0):
      res = {}
      prnt = False
      for p in pages.values():
        for b in p['blocks']:
          if (block_classes is None) or b['class'] in block_classes:
            txt = b['text']
            for lbl in self._labels_list:
              ma = self._labels_comp[lbl].findall(txt)
              pos = 0
              if len(ma) > 0:
                for k in self._labels[lbl]:
                    if not res.get(k):
                        res[k] = {}
                    res[k][lbl] = self.zero_if_none(res[k].get(lbl)) + len(ma)
      return res


    # Le get_crop_from_res visait à chercher un **titre** de BSV.
    # On le remplace par 3 fonctions :
    # * climbup_res : qui remonte l'arbre des concepts depuis les résultats,
    #   sans passer par les concepts ambigüs.
    # - choose_res : à partir du résultat de climbup, choisit un
    #   ou des concepts "main".
    # - fit_res : à partir des résultats et de choose_res, renvoie
    #   la liste des concepts atteints qui sont fils du (des) "main" concept(s).
    def climbup_res(self, result):
        if len(result) == 0:
            return None
        res = {}
        for k in result:
            res[k] = result[k]

        chg = True
        while chg:
            res2 = {}
            chg = False
            for k in res:
                if self._tree[k]['main'] or len(self._tree[k]['broader']) == 0:
                    res2[k] = self.zero_if_none(res2.get(k)) + res[k]
                else:
                    chg = True
                    l = self._tree[k]['nb_ambiguious_parents']
                    for b in self._tree[k]['broader']:
                        if not self._tree[k]['ambiguity']:
                            res2[b] = self.zero_if_none(res2.get(b)) + res[k]
            res = res2
        return res

    def choose_climbedup_result(self, res):
        if res is None or len(res) == 0:
            return None
        res2 = {}
        while len(res2) == 0 and len(res) > 0:
            m = max([res[k] for k in res])
            res3 = {}
            for k in res:
                if res[k] == m:
                    if self._tree[k]['main'] :
                        res2[k] = res[k]
                else:
                    res3[k] = res[k]
            res = res3
        return res2

    # À partir du résultat de get_labels_from_pages ou de get_labels_from_text
    # et du résultat de choose_climbedup_result(climbup_res(résultat)),
    # renvoie un dictionnaire des main crops sélectionnés, contenant chacun
    # la liste des concepts trouvés, fils du main crop en question.
    def fit_res(self, result, choosen):
        if choosen is None or len(choosen) == 0:
            return None
        res = {}
        for k in choosen:
            res[k] = []
            for r in result:
                if r in self._main_crop[k]['sons'] and r not in res[k]:
                    res[k].append(r)
        return res

    def get_deepest(self, result, choosen):
        if choosen is None or len(choosen) == 0:
            return None
        max_deep = 0
        for k in choosen:
            for r in result:
                if r in self._main_crop[k]['sons']:
                    max_deep = max(max_deep, self._tree[r]['deep'])
        res = []
        for k in choosen:
            for r in result:
                if r in self._main_crop[k]['sons'] \
                    and self._tree[r]['deep'] == max_deep and r not in res:
                        res.append(r)
        return res
    # Fin du truc qu'on teste


    def __search_in_text(self, crop_dict, txt):
        res = []
        for su in CROP_SUBSTITUTIONS:
            txt = " %s " % re.sub(su['regex'], su['sub'], txt)
        for k in crop_dict.keys():
            if crop_dict[k].get('regex') is not None:
                ma = crop_dict[k]['comp'].findall(txt)
                if len(ma) > 0:
                    res.append(k)
        return res

    def search_crop_in_text(self, txt):
        return self.__search_in_text(self._tree, txt)
    def search_main_crop_in_text(self, txt):
        return self.__search_in_text(self._main_crop, txt)
