# Évaluation des algorithmes d'indexation

L'évaluation s'effectue sur trois corpus de test, décrits dans [src/getIndex/Algo.md]().

Pour chaque corpus, il y a deux fichiers.

## crop.csv

Fichier csv dont le séparateur de colonnes est le point-virgule.

Contient, dans l'ordre des colonnes :

1. Le nom du fichier du BSV.
1. La date trouvée par getIndex.
1. Le(s) type(s) de culture(s) éventuellement trouvé(s) dans le nom du fichier.
1. Le(s) type(s) de culture(s) éventuellement trouvé(s) dans le texte
  de l'hyperlien lors du téléchargement du BSV.
1. Le(s) type(s) de culture(s) éventuellement trouvé(s) dans les titres,
  sous-titres, en-tête et pieds de page dans le contenu du BSV.
1. Le(s) type(s) de culture(s) éventuellement trouvé(s) dans le contenu du BSV.

## crop.verbose.txt

Contient les sorties verbeuses de l'exécution du programme, permettant
de suivre le processus d'extraction des types de culture dans le contenu
du BSV.

Les informations contenues dans ces fichiers sont décrites à la fin
de [src/getIndex/Algo.md]().

