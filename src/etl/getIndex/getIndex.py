#!/usr/bin/env python3
import sys
import io
import os
import re

import requests
import arrow
import json
import shutil
from datetime import datetime, date, timedelta, timezone


# https://unix.stackexchange.com/questions/238180/execute-shell-commands-in-python
import subprocess

PDF2BLOCS_PATH = '/home/phan/Boulot/Ontology/BSV/Tayeb/pdf2blocs/src/py/'
#PDF2BLOCS_PATH = '/home/famillezb/mnt/Boulot/Ontology/BSV/Tayeb/pdf2blocs/src/py/'

sys.path.append(PDF2BLOCS_PATH)
from p2b_functions import *

from Tools import *
from DateParser import *
from CropTree import *

# Exemple d'utilisation :
# find ~/Boulot/Ontology/BSV/tmp/Corpus/Tests/ -type f -name '*.pdf' | python getIndex.py

# +--------------------------------------------------------------+
# |                        CONSTANTES                            |
# +--------------------------------------------------------------+

######################### Debug ########################
DEBUG = 3

## Shell command to compute md5sum
CMD_MD5SUM = '/usr/sbin/md5sum'

####################### Fichiers #######################
## The place to copy BSVs. A substructure structure region/year will be created there.
DEST_PATH = '/home/phan/Boulot/Ontology/BSV/D2KAB/gitlab/d2kab/src/getIndex/test'

COPY_FILES = True
SAVE_PDF2BLOCKS = True
WRITE_TO_TRIPLESTORE = True
TTL_TO_STDOUT = True

UPDATE_SERVER = 'http://ontology.inrae.fr:3030/bsv_test/update'

## The define the path of file to download. SHould be in html/ and pdf/ directories. :
DEST_URL = 'http://ontology.inrae.fr/bsv/files/'

##################### Triplestores #####################
BASE_URI = "http://ontology.inrae.fr/bsv/"
## Requêtes depuis le serveur ontology.inrae.fr :
#DWNL_SERVER = 'http://ontology.irstea.fr:3030/bsv_dwnl/'

BSV_GRAPH = "Bulletin"
ANNOTATION_GRAPH = "Annotation"

### Pour une utilisation locale :
## Récupérer les données :
# ~/opt/share/apache-jena-fuseki-4.0.0/bin/s-get http://ontology.irstea.fr:3030/bsv_dwnl/ default > BSVs.ttl
## Démarrer un serveur local :
# cd ~/Boulot/Ontology/BSV/D2KAB/SB/bsvIndex
# ~/opt/share/apache-jena-fuseki-4.0.0/fuseki-server -q --file=BSVs.ttl /bsv_dwnl
#DWNL_SERVER = 'http://localhost:3030/bsv_dwnl/'
DWNL_SERVER = 'http://ontology.irstea.fr:3030/bsv_dwnl/'

DWNL_PREFIX = "prefix dwl: <http://ontology.irstea.fr/bsv_dwnl/>"

## For FrenchCropUsage :
# cd ~/Boulot/Ontology/FrenchCropUsage/frenchcropusage
# ~/opt/share/apache-jena-fuseki-4.0.0/fuseki-server -q --port 3031 --file=frenchCropUsage_latest.ttl /cropusage
#CROP_SPARQL_SERVER = 'http://localhost:3031/cropusage/'
CROP_SPARQL_SERVER = 'http://ontology.irstea.fr/frenchcropusage/'

CROP_PREFIX = 'http://ontology.inrae.fr/frenchcropusage/'
#CROP_PREFIX = 'http://ontology.irstea.fr/cropusage/'

### Prefixes
PREFIXES = {
'vespa': 'http://ontology.inrae.fr/bsv/ontology/',
'wd': 'http://www.wikidata.org/entity/',
'dc': 'http://purl.org/dc/elements/1.1/',
'oa': 'http://www.w3.org/ns/oa#',
'dcterms': 'http://purl.org/dc/terms/',
'dctypes': 'http://purl.org/dc/dcmitype/',
'dul': 'http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#',
'xsd': 'http://www.w3.org/2001/XMLSchema#',
'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
'prov': 'http://www.w3.org/ns/prov#',
'schema': 'http://schema.org/',
}


##################### Localisation #####################
## URL/URI des sources
#### PREFIX wd: <http://www.wikidata.org/entity/>
REG_SOURCES = {
 'draaf.auvergne-rhone-alpes.agriculture.gouv.fr': "Q18338206",
 'draaf.bourgogne-franche-comte.agriculture.gouv.fr': "Q18578267",
 'draaf.bretagne.agriculture.gouv.fr': "Q12130",
 'draaf.centre-val-de-loire.agriculture.gouv.fr': "Q13947",
 'draaf.corse.agriculture.gouv.fr': "Q14112",
 'draaf.grand-est.agriculture.gouv.fr': "Q18677983",
 'daaf.guadeloupe.agriculture.gouv.fr': "Q17012",
 'draaf.hauts-de-france.agriculture.gouv.fr': "Q18677767",
 'driaaf.ile-de-france.agriculture.gouv.fr': "Q13917",
 'daaf.mayotte.agriculture.gouv.fr': "Q17063",
 'draaf.normandie.agriculture.gouv.fr': "Q18677875",
 'draaf.nouvelle-aquitaine.agriculture.gouv.fr': "Q18678082",
 'draaf.occitanie.agriculture.gouv.fr': "Q18678265",
 'draaf.paca.agriculture.gouv.fr': "Q15104",
 'draaf.pays-de-la-loire.agriculture.gouv.fr': "Q16994",
}

## Regexp
#### Régions à partir de 2016
REGIONS_REGEX = [ 'auvergne.rh[ôo]ne.alpes', 'bourgogne.franche.comt[eé]',
  'bretagne', 'centre.val.de.loire', 'corse', 'grand.est',
  'hauts.de.france', 'ile.de.france', 'normandie', 'nouvelle.aquitaine',
  'occitanie', 'pays.de.la.loire', 'provence.alpes.c[ôo]te.d.azur',
  'guadeloupe', 'martinique', 'guyane', 'la.r[ée]union', 'mayotte']
REGIONS_NAMES = [ 'Auvergne-Rhône-Alpes', 'Bourgogne-Franche-Comté',
  'Bretagne', 'Centre-Val de Loire', 'Corse', 'Grand Est',
  'Hauts-de-France', 'Île-de-France', 'Normandie', 'Nouvelle-Aquitaine',
  'Occitanie', 'Pays de la Loire', 'Provence-Alpes-Côte d\'Azur',
  'Guadeloupe', 'Martinique', 'Guyane', 'La Réunion', 'Mayotte']
REGIONS_WIKIDATA = [ "Q18338206", "Q18578267", "Q12130", "Q13947",
  "Q14112", "Q18677983", "Q18677767", "Q13917", "Q18677875",
  "Q18678082", "Q18678265", "Q16994", "Q15104", "Q17012", "Q17054",
  "Q3769", "Q17070", "Q17063"]

#### Régions avant 2016
OLD_REGIONS_REGEX = [
  '[iî]le.de.france', 'champagne.ardenne', 'picardie', 'haute.normandie',
  'r[ée]gion.centre', 'basse.normandie', 'bourgogne', 'nord.pas.de.calais',
  'lorraine', 'alsace', 'franche.comt[ée]', 'pays.de.la.loire',
  'bretagne', 'poitou.charentes', 'aquitaine', 'midi.pyr[ée]n[ée]es',
  'limousin', 'rh[ôo]ne.alpes', 'auvergne', 'languedoc.roussillon',
  'provence.alpes.c[ôo]te.d.azur', 'corse', 'guadeloupe', 'martinique',
  'guyane', 'la.r[ée]union', 'mayotte'
]
OLD_REGIONS_NAMES = [
  'Île-de-France', 'Champagne-Ardenne', 'Picardie', 'Haute-Normandie',
  'Centre', 'Basse-Normandie', 'Bourgogne', 'Nord-Pas-de-Calais',
  'Lorraine', 'Alsace', 'Franche-Comté', 'Pays de la Loire',
  'Bretagne', 'Poitou-Charentes', 'Aquitaine', 'Midi-Pyrénées',
  'Limousin', 'Rhône-Alpes', 'Auvergne', 'Languedoc-Roussillon',
  'Provence-Alpes-Côte d\'Azur', 'Corse', 'Guadeloupe', 'Martinique',
  'Guyane', 'La Réunion', 'Mayotte'
]
OLD_REGIONS_WIKIDATA = [ "Q13917", "Q14103", "Q13950", "Q16961",
  "Q13947", "Q16954", "Q1173", "Q16987", "Q1137", "Q1142", "Q16394",
  "Q16994", "Q12130", "Q17009", "Q1179", "Q16393", "Q1190", "Q463",
  "Q1152", "Q17005", "Q15104", "Q14112", "Q17012", "Q17054",
  "Q3769", "Q17070", "Q17063"]

#### Régions à chercher AVANT les autres (p.ex on cherche Haute/Basse Normandie
#### avant de chercher Normandie — on ne se préoccupe pas des doublons, il y
#### en a de toutes façons entre les régions avant et après 2016).
PRE_REGIONS_REGEX=['haute.normandie','basse.normandie']
PRE_REGIONS_NAMES=['Haute-Normandie','Basse-Normandie']
PRE_REGIONS_WIKIDATA=["Q16961","Q16954"]

#### Régions à chercher APRÈS les autres (p.ex, si on n'a rien trouvé, on
#### peut chercher PACA par exemple)
POST_REGIONS_REGEX=['paca']
POST_REGIONS_NAMES=['Provence-Alpes-Côte d\'Azur']
POST_REGIONS_WIKIDATA=["Q15104"]

ALL_REGIONS = [re.compile(er, re.IGNORECASE) \
               for er in PRE_REGIONS_REGEX + REGIONS_REGEX + \
                         OLD_REGIONS_REGEX + POST_REGIONS_REGEX]
ALL_REGIONS_NAMES = PRE_REGIONS_NAMES + REGIONS_NAMES + OLD_REGIONS_NAMES + \
                    POST_REGIONS_NAMES
ALL_REGIONS_WIKIDATA = PRE_REGIONS_WIKIDATA + REGIONS_WIKIDATA + \
                       OLD_REGIONS_WIKIDATA + POST_REGIONS_WIKIDATA

REGION_BLOCKS_REGEX = ['chambre\W+r[ée]gionale\W+d.agriculture',
                        'chambre\W+d.agriculture']
REGION_BLOCKS = [re.compile(er, re.IGNORECASE) for er in REGION_BLOCKS_REGEX]


##################### Dates #####################
FROM = date(2008,1,1)
TO = date.today() - timedelta(days = 1)
LOCALE = 'fr_FR.UTF-8'

MONTH_RE = DateParser.MONTH_RE
MONTH_SUB = DateParser.MONTH_SUB
FILENAME_DT_FORMATS = [
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>\d\d?)\W(?P<month>\d\d?)\W(?P<year>\d{4})', 'fmt':('%d','%m','%Y')}, # 15 01 2017
  {'regex': r'\D(?P<year>\d{4})\W(?P<month>\d\d?)\W(?P<day>\d\d?)\D', 'fmt':('%d','%m','%Y')}, # 2017 01 15
  {'regex': r'(?P<day>\d\d?)/(?P<month>\d\d?)/(?P<year>\d{2})\D', 'fmt':('%d','%m','%y')}, # 15/01/17
  {'regex': r'20(?P<year>\d{2})(?P<month>\d{2})(?P<day>\d{2})\D', 'fmt':('%d','%m','%y')}, # 20170115
  {'regex': r'(?P<day>\d{2})(?P<month>\d{2})20(?P<year>\d{2})', 'fmt':('%d','%m','%y')}, # 15012017
  {'regex': r'\D(?P<day>\d{2})\W?(?P<month>\d{2})\W?(?P<year>\d{2})\D', 'fmt':('%d','%m','%y')}, # 150117
]
FILENAME_DT_FORMATS_IF_NONE = [
  {'regex': r'\D(?P<year>\d{2})\W?(?P<month>\d{2})\W?(?P<day>\d{2})\D', 'fmt':('%d','%m','%y')}, # 150117
]
TEXT_DT_FORMATS = [
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>\d\d?)\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{4})', 'fmt':('%d','%B','%Y')}, # 15 janvier 2017
  {'regex': r'(?P<day>1)\W?er\W?'+MONTH_RE+'\W?(?P<year>\d{2})\D', 'fmt':('%d','%B','%y')}, # 15 janvier 17
  {'regex': r'(?P<day>\d\d?)/(?P<month>\d\d?)/(?P<year>\d{2})\D', 'fmt':('%d','%m','%y')}, # 15/01/17
  {'regex': r'(?P<day>\d\d?)\W(?P<month>\d\d?)\W(?P<year>\d{4})', 'fmt':('%d','%m','%Y')}, # 15 01 2017
]

# Be careful : Date labels should be ordered so that the first
# is the best method, then decrease. This order is used for choosing
# the publication date.
# Note that the txt_most is after tstore, event if it's better ;
# if a date is found in text, it'll return the txt_first, so…
DATE_LABELS = ['txt_first', 'file', 'tstore', 'txt_most', 'download']

# % of exact dates for each method. Have been computed on test corpuses.
# Note that considering a difference of 1 week, the scores are better.
SCORES = {'txt_first':95 , 'file':93, 'tstore':85,
	'txt_most':89, 'download':0} # The score of download date have not
	# been evaluated, but it should be quite wrong…

OVERALL_SCORE = 95 # Overall score seems not better then txt_first,
                   # but it returns much more results…


# +--------------------------------------------------------------+
# |                      sparql_update                           |
# +--------------------------------------------------------------+
def sparql_update(query):
    query = { 'update': query }
    req = requests.post(UPDATE_SERVER, data=query)
    if (req.status_code != requests.codes.ok):
        return '%d - %s' % (req.status_code, req.text)
    return None

# +--------------------------------------------------------------+
# |                   find_in_triplestore                        |
# +--------------------------------------------------------------+
# find_in_triplestore("/path/to/file") will search into the triplestore
# used for BSD collect process a BSVDownload containing "/path/to/file"
# for predicate dwl:local_file. If not found, will try to find "to/file",
# then "file".
# Return a list of results, or None if none found.
def find_in_triplestore(path_file):
    file = path_file.split("/")
    l = 0
    headers = {'Accept':'application/json'}
    prev_s = None
    while l < len(file):
        l += 1
        fi = "/".join(file[-l:])
        query = {'query': """%s
SELECT ?bsv ?dt ?txt ?fi WHERE {
  ?bsv a dwl:BSVDownload ; dwl:datetime ?dt ; dwl:status "OK";
  dwl:has_text ?txt; dwl:local_file ?f .
  BIND("%s" AS ?fi)
  FILTER contains(?f, "%s") }
""" % (DWNL_PREFIX, fi, fi)}
        req = requests.get(DWNL_SERVER+'query',  params=query, headers=headers)
        if (req.status_code != requests.codes.ok):
            if (DEBUG > 0):
                print("SPARQL ERROR [%d] :\n%s" % (req.status_code, query['query']))
                print("")
        else:
            s = json.loads(req.text)
            if (len(s['results']['bindings']) == 1):
                return s['results']['bindings']
            if (len(s['results']['bindings']) == 0):
                return prev_s
            prev_s = s['results']['bindings']
    return None


# +--------------------------------------------------------------+
# |                      blocks_to_text                          |
# +--------------------------------------------------------------+
def blocks_to_text(blocks):
    txt = []
    for b in blocks:
        s = ''
        for l in b['lines']:
            if l['text'] is not None:
                if len(l['text']) > 0 :
                    if l['text'][-1] != '-':
                        s = "%s %s" % (s, l['text'])
                    else:
                        s = "%s%s" % (s, l['text'][:-1])
        txt.append(s)
    return txt

# +--------------------------------------------------------------+
# |                      pages_to_text                          |
# +--------------------------------------------------------------+
def pages_to_text(blocks, select = [i for i in range(BL_MISC+1)]):
    txt = []
    for p in pages.values():
        for b in p['blocks']:
            if b['class'] in select:
                txt.append(b['text'])
    return txt

# +--------------------------------------------------------------+
# |                         get_date                             |
# +--------------------------------------------------------------+
### Initialize date parsers
filename_dt_parser = DateParser(FILENAME_DT_FORMATS, LOCALE, MONTH_SUB, FROM,TO)
if TO >= date(2013,1,1):
    filename_dt_parser_if_none = DateParser(FILENAME_DT_FORMATS_IF_NONE,
                                        LOCALE, MONTH_SUB,
                                        max(FROM, date(2013,1,1)), TO)
else:
    filename_dt_parser_if_none = None
text_dt_parser = DateParser(TEXT_DT_FORMATS, LOCALE, MONTH_SUB, FROM,TO)

# Chooses a date for a BSV.
# May return None if no date is found.
def get_date(file, tstore, pages):
    # --- Search date in filename
    dt = { 'date': None, # The finally chosen date
            'file': None,  # If found in filename
            'tsore': None, 'download': None,  # Found during collecting
            'txt_most': None, 'txt_first': None } # In whole text
    fic = file.split("/")
    found = False
    i = 0
    while i < len(fic) and not found:
        i += 1
        filename = re.sub('_','-',fic[-i])
        dt['file'] = filename_dt_parser.getDates(filename,
                                mode = DateParser.GET_FIRST)
        if dt['file'] is not None:
            found = True
    if not found and filename_dt_parser_if_none is not None:
        dt['file'] = filename_dt_parser_if_none.getDates(fic[-1],
                                mode = DateParser.GET_FIRST)

    # --- Search date into txt value in triplestore (if date not found)
    if tstore is not None:
        for t in tstore:
            dt['tstore'] = text_dt_parser.getDates(t['txt']['value'],
                                mode = DateParser.GET_FIRST)
            dt['download'] = datetime.strptime(t['dt']['value'][:10],
                                "%Y-%m-%d").date()
            if dt['tstore'] is not None:
                break

    # --- Search date into text (if still not found)
    if pages is not None:
        bsv_text = pages_to_text(pages)

        # Search most used date
        res = {}
        for t in bsv_text:
            res = text_dt_parser.getDates(t, mode = DateParser.GET_ALL, res=res)
        dt['txt_most'] = text_dt_parser.getMost(res)

        # Search first date
        for t in bsv_text:
            dt['txt_first'] = text_dt_parser.getDates(t, mode = DateParser.GET_FIRST)
            if dt['txt_first'] :
                break

    # --- Choose returned date
    nb_none = sum([dt[i] is None for i in dt])
    if nb_none == len(dt):
        return None
    if nb_none <= 2: # One or two values : choose best
        for d in DATE_LABELS:
            if dt[d] is not None:
                dt['date'] = dt[d]
                dt['score'] = SCORES[d]
                return dt

    dist = {}
    for k in dt:
        dist[k] = None
    mini = None
    for i in dt:
        if dt[i]: # dt['date'] is None here.
            dist[i] = 0
            for j in dt:
                if i != j and dt[j]:
                    dist[i] += int(abs((dt[i] - dt[j]).days)/7)
            if not mini: mini = dist[i]
            if dist[i] < mini: mini = dist[i]

    for i in dt:
        if dt[i] and dist[i] == mini:
            dt['date'] = dt[i]
            dt['score'] = OVERALL_SCORE
            return dt

    if DEBUG > 1:
        print(">>> Error : get_date doesn't return any date !")
    return None


# +--------------------------------------------------------------+
# |                        get_region                            |
# +--------------------------------------------------------------+
# Return the Wikidata code of a french region name if a region name
# is found in the text given for argument, otherwise returns 0.
def find_region_in_text(text):
    for rx, insee in zip(ALL_REGIONS, ALL_REGIONS_WIKIDATA):
        if rx.search(text) is not None:
            return insee
    return 0

def get_region_from_pages(pages):
    for exp in REGION_BLOCKS:
        for p in pages.values():
            for b in p['blocks']:
                if exp.search(b['text']) is not None:
                    ch = ' '.join(exp.split(b['text'])[1:])
                    reg = find_region_in_text(ch)
                    if reg:
                        return reg
    # --- If no results, we search the most found region in the whole text
    res = {}
    max = 0
    wd_max = None
    for exp, wd in zip(ALL_REGIONS, ALL_REGIONS_WIKIDATA):
        for p in pages.values():
            for b in p['blocks']:
                l = len(exp.split(b['text'])) - 1
                if l > 0:
                    if res.get(wd):
                        res[wd] += l
                    else:
                        res[wd] = l
                    if res[wd] > max:
                        max = res[wd]
                        wd_max = wd
    return wd_max

def get_region_from_tstore(ts):
	if ts is None: return None
	# Extractions of http://<what's interesting>/...
	sp = ts['bsv']['value'].split('/') # Should be sp[2] but...
	for domain in sp:
		if len(domain.split('.')) > 1: break
	if REG_SOURCES.get(domain) is None: return None
	return REG_SOURCES[domain]

# +--------------------------------------------------------------+
# |                        get_md5sum                            |
# +--------------------------------------------------------------+
def get_md5sum(filename):
  basename = os.path.splitext(filename)[0]
  cmd = [CMD_MD5SUM, '%s.pdf' % basename]
  proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  o, e = proc.communicate()
  if (proc.returncode != 0):
    return None
  return o.decode('utf8')


# +--------------------------------------------------------------+
# |                           main                               |
# +--------------------------------------------------------------+
if (len(sys.argv) >= 1):
    if "--help" in sys.argv[1:]:
        print("Usage : python %s [<file1> [<file2> …]]" % sys.argv[0])
        print("  If no filename is given, it'll get them from standard input")
        print("  (so you can pipe the result of a find command for example)")
        print("")
        sys.exit(0)

if (len(sys.argv) == 1):
    inp = sys.stdin
else:
    ch = "\n".join(sys.argv[1:])
    inp = io.StringIO(ch)


## Get crop and compile crop parsers
ctree = None
try:
    ctree = CropTree(CROP_SPARQL_SERVER, CROP_PREFIX, DEBUG)
except Exception as ex:
    if DEBUG > 1:
        print(">>> Error : %s" % ex)
    ctree = None

if ctree is None:
    print("### Error getting crop and labels")
    sys.exit(-1)


while DEST_PATH[-1] == '/':
    DEST_PATH = DEST_PATH[:-1]
while DEST_URL[-1] == '/':
    DEST_URL = DEST_URL[:-1]

ttl_prefixes = sparql_prefixes = ''
for p in PREFIXES:
    ttl_prefixes += "@prefix %s: <%s>.\n" % (p, PREFIXES[p])
    sparql_prefixes += "prefix %s: <%s>\n" % (p, PREFIXES[p])
ttl_prefixes += '\n'
if TTL_TO_STDOUT:
    print(ttl_prefixes)

## Let's go
for line in inp:
    if line[-1] == '\n':
        line = line[:-1]
    if DEBUG > 2:
        print("======> %s" % line)

    # Get text structure.
    start_p2b = datetime.now()
    DISABLE_EXTRACTION = False # Extraction is sometimes very long,
      # and it can be useful to disable it during developpement process.
    if (not DISABLE_EXTRACTION) or SAVE_PDF2BLOCKS:
        blocks = get_pdftotext(line)
        p2h = get_pdftohtml(line)
    else :
        blocks = None
        p2h = None
    pages = None
    nb_words = 0
    if p2h is not None:
        fontspec = p2h['fonts']
        segments = p2h['segments']
        default_font_size = get_default_font_size(fontspec)
        guess_fonts(blocks, segments, fontspec)
        pages = guess_structure(blocks, fontspec, default_font_size)
        # Let's add a 'text' key to blocks
        for p in pages.values():
            for b in p['blocks']:
                txt = ''
                for l in b['lines']:
                    txt = '%s%s ' % (txt, l['text'])
                b['text'] = re.sub(r'\s+', ' ', txt.strip())
                nb_words += 1 + len(re.sub(r'\S', '', b['text']))
    if pages is None and DEBUG > 1 and not DISABLE_EXTRACTION:
        print("  *Structure extraction failed*")

    # --- Compute md5sum
    md5 = get_md5sum(line)
    if md5 is not None:
        md5 = md5.split(" ")[0]
        if DEBUG > 4:
            print("  md5sum : %s" % md5)

    # --- Search if filename exists in triplestore
    tstore = find_in_triplestore(line)
    if tstore is not None:
        if DEBUG > 4:
            print("  Found %d BSVDownload objects in triplestore" % len(tstore))
            for ts in tstore:
                for item in ['bsv', 'dt', 'txt', 'fi']:
                    print("  %s : %s" % (item, ts[item]['value']))
    else:
        if DEBUG > 4:
            print("  Not found in triplestore")

    # --- Get region from triplestore entry
    ts_region = text_region = None
    if tstore:
        ts_region = get_region_from_tstore(tstore[0])
        if DEBUG > 4:
            print("  Region : wd:%s" % ts_region)
    if ts_region is None and pages:
        text_region = get_region_from_pages(pages)
        if DEBUG > 4:
            print("  Region found using full text search : wd:%s" % text_region)


	# --- Get date
    dt_bsv = get_date(line, tstore, pages)
    if dt_bsv:
        textdt = dt_bsv['date'].strftime("%d/%m/%Y")
    if DEBUG > 4:
        if dt_bsv:
            print("  Date found : %s [score : %s%%]" % (textdt, dt_bsv['score']))
        else:
            print("  No date found")

    # --- Search crop in text
    #
    a_crop = c_crop = None
    if pages is not None:
        a_crop = ctree.get_labels_from_pages(pages, None, verbosity = DEBUG)
        if DEBUG > 4:
            if a_crop is not None and len(a_crop) > 0:
                print("  --[  Crop Found  ]--> %s" % a_crop)
            else:
                print("  --[ No crop found ]--")
    else:
        if DEBUG > 4:
            print("  =-=[ No text ]=-=")

    # --- file operations
    basename = slugify(".".join(line.split("/")[-1].split('.')[0:-1]))
    if DEBUG > 4:
        print("  Basename : %s" % basename)

    # ----- copy file

    reg = 'NotLocated'
    if ts_region:
        reg = ("%s" % ts_region)
    elif text_region:
        reg = ("%s" % text_region)
    yr = "NoDate"
    if dt_bsv:
        yr = dt_bsv['date'].strftime("%Y")
    elif tstore:
        yr = "%s" % tstore['dt'][0:4]

    pdf_dest_dir = "%s/pdf/%s/%s" % (DEST_PATH, reg, yr)
    html_dest_dir= "%s/html/%s/%s" % (DEST_PATH, reg, yr)
    # All /path/to/file/and/file.type :
    pdf_path_file = "%s/%s.pdf" % (pdf_dest_dir, basename)
    html_path_file= "%s/%s.html"% (html_dest_dir,basename)


    uri = "%sresources/%s/%s/%s" % (BASE_URI, reg, yr, basename)
    uri_annotation = "%sresources/annotation/%s/%s/%s" % (BASE_URI, reg, yr, basename)
    pdf_uri = "%s/pdf/%s/%s/%s.pdf" % (DEST_URL, reg, yr, basename)
    html_uri ="%s/html/%s/%s/%s.html" % (DEST_URL, reg, yr, basename)

    copied = True
    if DEBUG > 4:
        print("  Destination directory : %s" % pdf_dest_dir)
    if COPY_FILES:
        try:
            os.makedirs(pdf_dest_dir, exist_ok=True)
            i = 0
            while os.path.exists(pdf_path_file):
                i += 1
                pdf_path_file = "%s/%s_%03d.pdf" % (pdf_dest_dir, basename, i)
                pdf_uri = "%s/pdf/%s/%s/%s_%03d.pdf" % (DEST_URL, reg, yr,
                                                        basename, i)
                html_path_file = "%s/%s_%03d.html" % (html_dest_dir,basename, i)
                html_uri ="%s/html/%s/%s/%s_%03d.html" % (DEST_URL, reg, yr,
                                                        basename, i)
            shutil.copy2(line, pdf_path_file)
        except Exception as e:
            copied = False
            if DEBUG > 0:
                print("Exception occured, copying %s" % line)
                print("%s" % e)

    # ----- pdf2blocks output
    if pages and SAVE_PDF2BLOCKS:
        if DEBUG > 4:
            print("  Writing %s/%s.html" % (html_dest_dir, basename))
        try:
            os.makedirs(html_dest_dir, exist_ok=True)
            out = open(html_path_file, "w")
            print_html(pages, fontspec, out)
            out.close()
            del out
        except Exception as e:
            copied = False
            if DEBUG > 0:
                print("Exception occured, creating %s" % html_path_file)
                print("%s" % e)

    # --- Write ttl
    ttl = "\n<%s> a vespa:Bulletin ;\n" % uri
    if dt_bsv:
        ttl += "  dcterms:date \"%s\"^^xsd:date ;\n" % dt_bsv['date']
        ttl += "  vespa:dateExtractionQuality \"%d\"^^xsd:integer ;\n"%dt_bsv['score']
    if tstore and len(tstore) > 0:
        ttl += "  dcterms:description \"%s\"@fr ;\n" % tstore[0]['txt']['value']
    if ts_region:
        ttl += "  dcterms:spatial wd:%s ;\n" % ts_region
    elif text_region:
        ttl += "  dcterms:spatial wd:%s ;\n" % text_region

    if pages and SAVE_PDF2BLOCKS:
        ttl += "  dul:isRealizedBy <%s_html> ;\n" % uri
    ttl += "  dul:isRealizedBy <%s_pdf> .\n\n" % uri

    ttl += "<%s_pdf> a dctypes:Text, prov:Entity ;\n" % uri
    ttl += "  dc:language \"fr-FR\" ;\n"
    ttl += "  dc:format \"application/pdf\" ;\n"
    ttl += "  oa:textDirection oa:ltr ;\n"
    if tstore and len(tstore) > 0:
        ttl += "  prov:wasGeneratedBy <%s_collect> ;\n" % uri
    ttl += "  schema:url <%s> .\n\n" % pdf_uri
    if tstore and len(tstore) > 0:
        ttl += "<%s_collect> a prov:Activity ;\n" % uri
        ttl += "  prov:endedAtTime \"%s\"^^xsd:datetime ;\n" % tstore[0]['dt']['value']
        ttl += "  prov:used <%s> ;\n" % tstore[0]['bsv']['value']
        ttl += "  prov:generated <%s_pdf> ;\n" % uri
        ttl += "  prov:wasAssociatedWith vespa:collect .\n\n"


    if pages and SAVE_PDF2BLOCKS:
        ttl += "<%s_html> a dctypes:Text, prov:Entity ;\n" % uri
        ttl += "  dc:language \"fr-FR\" ;\n"
        ttl += "  dc:format \"text/html\" ;\n"
        ttl += "  oa:textDirection oa:ltr ;\n"
        ttl += "  prov:wasGeneratedBy <%s_transPdf2Html> ;\n" % uri
        ttl += "  schema:url <%s> ;\n" % html_uri
        ttl += "  rdfs:comment \"%d words\"@en .\n\n" % nb_words
        ttl += "<%s_transPdf2Html> a prov:Activity ;\n" % uri
        ttl += "  prov:startedAtTime \"%s\"^^xsd:datetime ;\n" % start_p2b.replace(microsecond=0).isoformat()
        ttl += "  prov:endedAtTime \"%s\"^^xsd:datetime ;\n" % datetime.now().replace(microsecond=0).isoformat()
        ttl += "  prov:used <%s_pdf> ;\n" % uri
        ttl += "  prov:generated <%s_html> ;\n" % uri
        ttl += "  prov:wasAssociatedWith vespa:pdf2blocks .\n\n"

    if TTL_TO_STDOUT:
        print(ttl)
    if WRITE_TO_TRIPLESTORE:
        query = sparql_prefixes+"""
INSERT DATA { GRAPH <%sbulletins> {
%s
}}""" % (BASE_URI, ttl)
        err = sparql_update(query)
        if err:
            print("### ERROR : %s" % err)
            if DEBUG > 0:
                print("=== Query :")
                print(query)
                print()


