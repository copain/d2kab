# À partir du (ou des) fichier(s) pdf de BSV(s) collecté pendant le projet D2KAB,
# crée le code turtle-rdf qui correspond à destination du projet D2KAB.
# Ce code est envoyé sur la sortie standard.
# Les préfixes ne sont pas générés (ils doivent être dans ttl/prefixes.ttl)

import sys
import io
import os
import re

import requests
import json
import csv
from datetime import datetime, date, timedelta, timezone
from etl_functions import *
from config import *


# +--------------------------------------------------------------+
# |                          CONFIG                              |
# +--------------------------------------------------------------+

CORPUS_NAME = 'corpusAuto'

# Si not None, alors les fichiers csv sont chargés pour y chercher des dates
# de publication extraites manuellement.
#CSV_DATE_FILES = None
CSV_DATE_FILES = [
  '/home/stef/Boulot/Devel/d2kab/src/etl/py/data/datesManuD2kab.csv',
  '/home/stef/Boulot/Devel/d2kab/src/etl/py/data/datesManuAlea.csv',]
CSV_DATE_DELIMITER = ','
CSV_DATE_COLUMNS = ['bsv', 'date'] # Other than 'date' and 'bsv' will be ignored
CSV_GET_BSV = lambda s : os.path.splitext(os.path.basename(s))[0]
CSV_GET_XSD_DATE = lambda s : '-'.join([s.split('/')[x] for x in [2,1,0]])



# +--------------------------------------------------------------+
# |                        CONSTANTES                            |
# +--------------------------------------------------------------+
REGIONS = {
    'auvergne-rhone-alpes' : 'Q18338206',
    'bourgogne-franche-comte' : 'Q18578267',
    'bretagne' : 'Q12130',
    'centre-val-de-loire' : 'Q13947',
    'corse' : 'Q14112',
    'grand-est' : 'Q18677983',
    'hauts-de-france' : 'Q18677767',
    'ile-de-france' : 'Q13917',
    'normandie' : 'Q18677875',
    'nouvelle-aquitaine' : 'Q18678082',
    'occitanie' : 'Q18678265',
    'paca' : 'Q15104',
    'pays-de-la-loire' : 'Q16994',
    'guadeloupe' : 'Q17012',
    'mayotte' : 'Q17063',
}


# +--------------------------------------------------------------+
# |                           main                               |
# +--------------------------------------------------------------+
region_from_url = re.compile(r'https?://[^\.]+\.([^\.]+)\.agriculture')
dates_collectes = []

IS_TEST_CORPUS = False
DEST_PATH = None
if (len(sys.argv) >= 1):
  if "--help" in sys.argv[1:]:
      print("Usage : python %s [options] [<file1> [<file2> …]]" % sys.argv[0])
      print("  If no filename is given, it'll get them from standard input")
      print("  (so you can pipe the result of a find command for example).")
      print('Options :')
      print("  --corpus_test={Alea|D2KAB} : The given files are included in a test corpus.")
      print("            The test corpus IRI will be ex:resources/corpusTest<Value>")
      print("            and should be declared in collections.ttl.")
      print("  --copy_bsv=/path/to/dest : Do a copy of the pdf files,")
      print("                  organized the same way as their URL into dest directory.")
      print("  --debug : Prints step by step process.")
      print("            The output should not be piped to doSparqlUpdate.py.")
      print("")
      sys.exit(0)

  DEBUG = ("--debug" in sys.argv[1:])
  if DEBUG :
    sys.argv.remove("--debug")

  for arg in sys.argv:
    if arg.startswith("--corpus_test"):
      if not arg.startswith("--corpus_test="):
        print("ERROR : --corpus_test option's syntax is --corpus_test=<Test corpus name>")
        sys.exit(-1)
      IS_TEST_CORPUS = True
      TESTCORPUS_NAME = 'corpusTest'+('='.join(arg.split('=')[1:]))
      sys.argv.remove(arg)
      break

  for arg in sys.argv: # remove/break avoids it to be treated within --corpus-test loop.
    if arg.startswith("--copy_bsv"):
      if not arg.startswith("--copy_bsv="):
        print("ERROR : --copy_bsv option's syntax is --copy_bsv=/path/to/dest")
        sys.exit(-1)
      DEST_PATH = '='.join(arg.split('=')[1:])
      sys.argv.remove(arg)
      break

if DEST_URL[-1] != '/':
    DEST_URL += '/'
if DEST_PATH == 'None': DEST_PATH = None
if DEST_PATH is not None:
  DEST_PATH = os.path.realpath(os.path.expanduser(DEST_PATH)) + '/'

if DEBUG:
  print("[DEBUG] Parameters :")
  print("[DEBUG]   DEBUG is ON")
  print("[DEBUG]   DEST_PATH = %s" % DEST_PATH)
  print("[DEBUG]   DEST_URL = %s" % DEST_URL)
  if IS_TEST_CORPUS:
    print("[DEBUG]   TESTCORPUS_NAME = %s" % TESTCORPUS_NAME)
  else:
    print("[DEBUG]   Is not a test corpus.")


dates_manu = None
if CSV_DATE_FILES is not None:
    dates_manu = {}
    for file in CSV_DATE_FILES:
        with open(file, newline='') as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=CSV_DATE_COLUMNS,
                delimiter=CSV_DATE_DELIMITER)
            for row in reader:
                dates_manu[CSV_GET_BSV(row['bsv'])] = row['date']

prefixes = load_prefixes(PREFIXES)
if prefixes is None:
    ex = EX_PREFIX
else:
    ex = prefixes['ex']

#organisation, conversion = get_current_acivity_names()
#print(create_current_activity())

if (len(sys.argv) == 1):
    inp = sys.stdin
else:
    ch = "\n".join(sys.argv[1:])
    inp = io.StringIO(ch)

BSV_written = 0
for line in inp:
    if line[-1] == '\n':
        line = line[:-1]
    if line[-1] == '*': # find command adds ugly trailing '*'…
        line = line[:-1]
    bsv_name = os.path.splitext(os.path.basename(line))[0]
    if DEBUG:
        print("[DEBUG] ===== BSV_NAME : %s" % bsv_name)

    rb = sparql_query(COLLECT_ENDPOINT+'/query',
"""prefix dwl: <http://ontology.irstea.fr/bsv_dwnl/>
SELECT ?bsv ?dt ?txt WHERE {
?bsv a dwl:BSVDownload ; dwl:datetime ?dt ; dwl:status "OK";
dwl:local_file ?f .
OPTIONAL { ?bsv dwl:has_text ?txt }
FILTER contains(?f, "%s") }""" % bsv_name)

    if rb is not None:
        if (len(rb) == 0):
            sys.stderr.write("=ERROR=====> %s :\n" % bsv_name)
            sys.stderr.write("  BSV non trouvé.\n")
        else:
            ## Dans le cas d'un BSV téléchargé en http ET en https,
            ## le second écrase le premier et on n'aura qu'un seul 
            ## fichier, ce qui est un comportement qu'on estime
            ## convenable.
            ## À noter que les préfixes région/année ont disparu
            ## lors du passage à xR2RML, ce qui peut mener à des
            ## erreurs si deux BSV ont exactement le même nom de
            ## fichier. L'expérience semble montrer que ceci n'est
            ## le cas que pour les notes nationales. À VÉRIFIER.
            rb = rb[0]
            date_collecte = rb['dt']['value'].split('T')[0]
            if rb.get('txt') is not None:
                hyperlink_text = rb['txt']['value']
            else:
                hyperlink_text = rb['bsv']['value'].split('/')[-1]
            if re.match('.*[?.!]$', hyperlink_text):
              hyperlink_text = hyperlink_text[:-1]          
            download_url  = rb['bsv']['value']
            region = region_from_url.search(download_url)
            if region is None:
                wd_region = None
            else:
                region = region[1]
                wd_region = REGIONS.get(region)
            if wd_region is None:
                sys.stderr.write("=ERROR==> Région non trouvée dans l'URL %s\n" % download_url)

            date = None
            if dates_manu.get(bsv_name) is not None:
                try:
                    date = CSV_GET_XSD_DATE(dates_manu[bsv_name])
                except Exception as e:
                    pass
            if DEBUG:
                if date is not None:
                    print("[DEBUG]   date : %s, relevée manuellement." % date)
                else:
                    print("[DEBUG]   Pas de date de publication (pour le moment)" % date)
                print("[DEBUG]   reg  : %s" % region)
                print("[DEBUG]   desc : %s" % hyêrlink_text)

            ### SHOULD call pdf2blocks here, and extract date

            d2kab_basename = get_d2kab_basename(line)
            if date is not None:
                d2kab_bsvname = get_d2kab_bsvname(line, wd_region, date)
            else :
                d2kab_bsvname = get_d2kab_bsvname(line, wd_region, date_collecte)
            url_pdf = "%spdf/%s.pdf" % (DEST_URL, d2kab_bsvname)
            url_html = "%shtml/%s.html" % (DEST_URL, d2kab_bsvname)

            if DEST_PATH is not None:
                dest = "%spdf/%s.pdf" % (DEST_PATH, d2kab_bsvname)
                if DEBUG:
                    print("[DEBUG]   copy to %s" % dest)
                d = file_copy(line, dest)
                if d != dest:
                    url_pdf = "%s%s" % (DEST_URL, d[len(DEST_PATH):])
                    num = re.findall(r'_[0-9]+\.pdf', d)[-1][1:4]
                    url_html = "%shtml/%s_%s.html" % (DEST_URL, d2kab_bsvname, num)

            if TEST_URL:
                exists_pdf = url_exists(url_pdf)
                exists_html = url_exists(url_html)
                if DEBUG:
                    print("[DEBUG]   %s exists : %s" % (url_pdf, exists_pdf))
                    print("[DEBUG]   %s exists : %s" % (url_html, exists_html))
            else:
                if DEBUG:
                    print("[DEBUG]   url_pdf : %s" % (url_pdf))
                    print("[DEBUG]   url_html : %s" % (url_html))

            # ================================
            # ===== Now write turtle-rdf =====
            # ================================
            bsv_uri = "%sresources/%s" % (ex, d2kab_basename)
            dt = ''.join(date_collecte.split('-')) # YYYYMMDD
            ttl = ''
            ttl += "## --- GRAPH ex:graph/activities --- ##\n"
            if date_collecte not in dates_collectes:
                dates_collectes.append(date_collecte)
                ttl += "ex:resources/collecte%s a prov:Activity ;\n" % dt
                ttl += '  prov:startedAtTime "%s"^^xsd:date ;\n' % date_collecte
                ttl += '  prov:endedAtTime "%s"^^xsd:date ;\n' % date_collecte
                dt_fr = '/'.join([date_collecte.split('-')[x] for x in [2,1,0]])
                ttl += '  rdfs:comment "Activité démarrée le %s ' % dt_fr
                ttl += 'avec l\'outil de collecte automatique des BSV."@fr ;\n'
                ttl += '  prov:wasAssociatedWith ex:resources/draafWebCrawler_1_0'
                ttl += ' .\n'

            bsv_uri = "%sresources/%s" % (ex, d2kab_basename)

            ttl += 'ex:resources/collecte%s' % dt
            ttl += '  prov:generated <%s_pdf> .\n' % bsv_uri

            ttl += "## --- GRAPH ex:graph/corpus --- ##\n"
            ttl += "ex:resources/%s " % CORPUS_NAME
            ttl += "prov:hadMember <%s> .\n" % bsv_uri
            if IS_TEST_CORPUS:
                ttl += "ex:resources/%s " % TESTCORPUS_NAME
                ttl += "prov:hadMember <%s> .\n" % bsv_uri
            ttl += "## --- GRAPH ex:graph/bsv --- ##\n"
            ttl += "<%s> a d2kab:Bulletin, prov:Entity ;\n" % bsv_uri
            ttl += '  dct:description "%s"@fr ;\n' % hyperlink_text
            if wd_region is not None:
                ttl += '  dct:spatial wd:%s ;\n' % wd_region
            if date is not None:
                ttl += '  dct:date "%s"^^xsd:date ;\n' % date
                if len(date) > 7: # date could be '' but should be 'YYYY-MM-DD'
                    ttl += '  d2kab:dateExtractionQuality "100 %"^^cdt:dimensionless ;\n'
            ttl += '  dul:isRealizedBy <%s_pdf> .\n' % bsv_uri
            ttl += '<%s_pdf> a prov:Entity, dct:Text,' % bsv_uri
            ttl += ' schema:DigitalDocument ;\n'
            if TEST_URL and not exists_pdf:
                ttl += "  # WARNING: URL doesn't exist (yet) :\n"
            ttl += '  schema:url <%s> ;\n' % url_pdf
            ttl += '  schema:isBasedOn <%s> ;\n' % download_url
            ttl += '  rdfs:comment "Entité qui représente une copie, '
            ttl += 'stockée sur le serveur TSCF, du «%s»."@fr ;\n' % hyperlink_text
            ttl += '  prov:wasGeneratedBy ex:resources/collecte%s;\n' % dt
            ttl += '  dce:language "fr-FR" ;\n'
            ttl += '  dce:format "application/pdf" ;\n'
            ttl += '  oa:textDirection oa:ltr .\n'

            print(ttl)

            BSV_written += 1
            if (BSV_written % UPDATE_EVERY_N_BSV) == 0:
                print("## --- UPDATE --- ##")

