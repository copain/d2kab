import sys
import io
import os
import os.path
import re
import requests
import subprocess
from multiprocessing import Pool, Lock
from datetime import datetime, date, timedelta, timezone
from etl_functions import *
from config import *

# +--------------------------------------------------------------------+
# |                           Config                                   |
# +--------------------------------------------------------------------+
PDF2BLOCS_PATH = '/home/stef/Boulot/Devel/pdf2blocs/src/py'
GIT_URL = 'https://gitlab.irstea.fr/copain/pdf2blocs/'

ACTIVITY_COMMENT = 'Activité de conversion des pdf en html ' \
  +'avec l\'outil pdf2blocks réalisée en %s ' % date.today().year \
  +'sur les corpus de tests.'
# +--------------------------------------------------------------------+
# |                         Functions                                  |
# +--------------------------------------------------------------------+
# Renvoie le code ttl de l'activité de conversion
def get_activity(dt_now, path = PDF2BLOCS_PATH, git_url=GIT_URL):
  if git_url[-1] != '/': git_url += '/'
  time_now = datetime.now().isoformat(timespec='minutes')

  # Get latest commit hash and date for running pdf2blocks.
  cmd = ['git', '-C', path, 'log', '-n', '1',
    "--pretty=%H*%ad", '--date=iso-strict', ]
  proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  o, e = proc.communicate()
  if (proc.returncode != 0):
    sys.stderr.write("[ERROR] Running git log on %s:\n" % path)
    sys.stderr.write("%s\n" % e.decode('utf8'))
    sys.exit(-1)

  ret = o.decode('utf8')
  git_hash, git_date=ret.strip().split('*')


  # Get latest tag value
  cmd = ['git', '-C', path, 'log', '-n', '1', "--pretty=%D", "--tags", ]
  proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  o, e = proc.communicate()
  git_version = ''
  if (proc.returncode == 0):
    git_version = o.decode('utf8')
    git_version = re.findall(r'tag: ([\w.]+)', git_version)
    if (len(git_version) > 0):
      git_version = git_version[0]
    else:
      git_version = ''
  else:
    if DEBUG:
      print("[DEBUG] Error searching pdf2blocs version : %s" % e.decode('utf8'))
  
  if DEBUG:
    print("[DEBUG] Git hash : %s" % git_hash)
    print("[DEBUG] Git date : %s" % git_date)  
    print("[DEBUG] Git tag  : %s" % git_version)  
  
  ret = '## --- GRAPH ex:graph/agents --- ##\n'
  ret += 'ex:/resources/pdf2blocks_%s a prov:SoftwareAgent,' % git_hash[:8]
  ret += ' schema:CreativeWork ;\n'
  if len(git_version) > 0:
    ret += '  schema:version "%s" ;\n' % git_version
  ret += '  schema:url <%stree/%s> ;\n' % (git_url, git_hash)
  ret += '.\n\n'

  ret += '## --- GRAPH ex:graph/activities --- ##\n'
  ret += 'ex:resources/conversion_%s a prov:Activity ;\n' % dt_now
  ret += '  prov:startedAtTime "%s"^^xsd:dateTime ;\n' % time_now
  ret += '  prov:wasAttributedTo ex:resources/stephan ;\n'
  ret += '  rdfs:comment "%s"@fr ;\n.\n\n' % ACTIVITY_COMMENT

  return ret


# === Define function for converting BSVs
# Should be called with a line of SPARQL results of query
# «SELECT ?bsv ?url ?desc WHERE …»
def convert_bsv(rb_row):
  global SUFFIX, DEST_PATH, LOCK, NB_UPDATES, DT_NOW, FORCE_SPARQL
  bsv = rb_row['bsv']['value']
  url = rb_row['url']['value']
  nb_words = None
  
  url_parts = url.split('/')
  if len(url_parts) < 4:
    if DEBUG:
      LOCK.acquire()
      print("[DEBUG] ERROR: %s is not a valid BSV URL" % url)
      LOCK.release()
    return -1

  dest_url = '/'.join(url_parts[:-4])+'/html'
  bsv_name = get_d2kab_bsvname(url_parts[-1],
                url_parts[-3], url_parts[-2])

  if DEBUG:
    LOCK.acquire()
    if DEST_PATH is None:
      print("[DEBUG] <%s> has no html realization." % bsv)
      print("[DEBUG]   URL : %s" % url)
    else:
      print("[DEBUG] Starting conversion : <%s>" % bsv_name)
    LOCK.release()
    
  # if DEST_PATH is None, we don't write html files.
  if (DEST_PATH is None) and (not FORCE_SPARQL):
    return -2 
  if DEST_PATH is not None:
    pdf_file = DEST_PATH+'/pdf/'+bsv_name+'.pdf'
    if not os.path.exists(pdf_file):
      if DEBUG:
        LOCK.acquire()
        print("[DEBUG] WARNING: file %s not found. Aborting." % pdf_file)
        LOCK.release()
      return -4
        # Other way : try to download from URL. *TODO*
    
    html_file = DEST_PATH+'/html/'+bsv_name+SUFFIX+'.html'
    #print("=====> ##### %s" % html_file)
    html_dest_dir = '/'.join(html_file.split('/')[:-1])
    try:
      os.makedirs(html_dest_dir, exist_ok=True)
    except Exception as e:
      if DEBUG:
        LOCK.acquire()
        print("[DEBUG] ERROR creating directory %s. Aborting." % html_dest_dir)
        LOCK.release()
      return -8
  
    html_content, nb_words = get_pdf2html(pdf_file)
    if DEST_PATH is None:
      nb_words = None
    try:
      out = open(html_file, "w")
      out.write(html_content)
      out.close()
    except Exception as e:
      if DEBUG:
        LOCK.acquire()
        print("[DEBUG] ERROR writing file %s. Aborting." % html_file)
        LOCK.release()
      return -16

  # TODO: write nb_words value.
  desc = rb_row['desc']['value']
  ttl = "## --- GRAPH ex:graph/bsv --- ##\n"
  ttl += "<%s> dul:isRealizedBy <%s_html> .\n" % (bsv, bsv)
  ttl += "<%s_html> a prov:Entity, dct:Text," % bsv
  ttl += " schema:DigitalDocument ;\n"
  ttl += "  prov:wasDerivedFrom <%s_pdf> ;\n" % bsv
  ttl += '  rdfs:comment "Entité qui représente le fichier html'
  ttl += ' résultat d\'une conversion du fichier pdf «%s»"@fr' % desc
  ttl += ' ;\n'
  ttl += '  dce:language "fr-FR" ;\n'
  ttl += '  dce:format "text/html" ;\n'
  ttl += '  oa:textDirection oa:ltr ;\n'
  if nb_words is not None:
    ttl += '  frac:total "%d"^^xsd:integer ;\n' % nb_words
  ttl += '  schema:url <%s/%s%s.html> .\n\n' % (dest_url,bsv_name,SUFFIX)
  ttl += "## --- GRAPH ex:graph/activities --- ##\n"
  ttl += 'ex:resources/conversion_%s prov:generated' % DT_NOW
  ttl += ' <%s_html> .\n\n' % bsv

  LOCK.acquire()
  if DEBUG:
    print("[DEBUG] Conversion finished : <%s>" % bsv_name)
  NB_UPDATES += 1
  print(ttl)
  if (NB_UPDATES % UPDATE_EVERY_N_BSV) == 0:
    print("## --- UPDATE --- ##")
  LOCK.release()
  return 0


# +--------------------------------------------------------------------+
# |                                                                    |
# |                             MAIN                                   |
# |                                                                    |
# +--------------------------------------------------------------------+
if __name__ != '__main__':
  sys.exit(-1)
sys.path.append(PDF2BLOCS_PATH)
from p2b_functions import get_pdf2html

DT_NOW = date.today().strftime('%Y%m%d')
DEST_PATH = None

## === Get Commandline arguments
if "--help" in sys.argv[1:]:
      print("Usage : python %s [options]" % sys.argv[0])
      print("""
Options :
  --dest_path=/path/to/dest : Where to store the html files,
        organized the same way as their URL into dest directory.
        Without this option, it only creates the prov:Entities into
        the triplestore, assuming files have already been converted.
        IMPORTANT : The pdf source files should already be 
        at the same place (path), in a subdirectory called 'pdf'.
        The generated html BSVs will be stored in another subdirectory
        called 'html'.
  --suffix=<suffix> : Adds a suffix to html files and html realizations.
        It makes possible to have different html realizations of a BSV,
        to compare different html converters, for example.
  --proc=<nb of processes> : Does multiprocessing html conversion.
        Html conversion may be a quite long process, and computers
        have multithreading capabilities nowadays. The value given
        for proc is the maximum number of html convertions executed
        at the same time.
  --force_sparql : Does the SPARQL UPDATE queries even if dest_path
        is not set. To be used in case the html conversion have already
        been done and triplestore needs to be rebuilt.
  --debug : Prints step by step process.
""")
      sys.exit(0)

DEBUG = ("--debug" in sys.argv[1:])
if DEBUG :
  sys.argv.remove("--debug")

FORCE_SPARQL = ("--force_sparql" in sys.argv[1:])
if FORCE_SPARQL:
  sys.argv.remove("--force_sparql")

for arg in sys.argv:
  if arg.startswith("--dest_path"):
    if not arg.startswith("--dest_path="):
      print("ERROR : --dest_path option's syntax is --dest_path=/path/to/dest")
      sys.exit(-1)
    DEST_PATH = '='.join(arg.split('=')[1:])
    sys.argv.remove(arg)
    break

SUFFIX = ''
for arg in sys.argv:
  if arg.startswith("--suffix"):
    if not arg.startswith("--suffix="):
      print("ERROR : --suffix option's syntax is --suffix=<suffix>")
      sys.exit(-1)
    SUFFIX = '='.join(arg.split('=')[1:])
    sys.argv.remove(arg)
    break

PROC = 1
for arg in sys.argv:
  if arg.startswith("--proc"):
    if not arg.startswith("--proc="):
      print("ERROR : --proc option's syntax is --proc=<nb of processes>")
      sys.exit(-1)
    PROC = int(arg.split('=')[-1])
    PROC = max(1, PROC)
    sys.argv.remove(arg)
    break

# Used to make only one process write to standard output at a time :
LOCK = Lock() 
# When locked to write a sparql update query, update this :
NB_UPDATES = 1

## === Adjust some parameter values
# We don't use DEST_URL. We'll get it from dataset.
#if DEST_URL[-1] != '/': DEST_URL += '/'
if SPARQL_SERVER[-1] == '/': SPARQL_SERVER = SPARQL_SERVER[:-1]
if BSV_DATASET[-1] == '/': BSV_DATASET = BSV_DATASET[:-1]
if BSV_DATASET[0] != '/': BSV_DATASET = '/'+BSV_DATASET
if DEST_PATH == 'None': DEST_PATH = None
if DEST_PATH is not None:
  DEST_PATH = os.path.realpath(os.path.expanduser(DEST_PATH)) + '/'


## === Get BSVs without html realization, from triplestore
prefixes = load_prefixes(PREFIXES)
ttl_prefixes = ''
for p in prefixes:
  ttl_prefixes = "%sPREFIX %s: <%s>\n" % (ttl_prefixes, p, prefixes[p])
query="""
SELECT distinct ?bsv ?url ?desc WHERE { 
GRAPH <http://ontology.inrae.fr/bsv/graph/bsv> {
  ?bsv a d2kab:Bulletin ;
       dul:isRealizedBy ?p ;
       dct:description ?desc .
  ?p dce:format "application/pdf" ;
      schema:url ?url .
  FILTER NOT EXISTS { [] prov:wasDerivedFrom ?p ; dce:format "text/html" }
}}"""

# TODO: SUFFIX detection in html realizations

rb = sparql_query(SPARQL_SERVER+BSV_DATASET+'/query', ttl_prefixes+query)
if rb is None:
  sys.error.write("ERROR while executing SPARQL query.\n")
  sys.exit(-1);
if (len(rb) == 0):
  if DEBUG:
    print("[DEBUG] No BSV without html realization found.")
else:
  # === Declare activity.
  activity = get_activity(DT_NOW)
    
  # === Convert and write html version of BSVs
  with Pool(PROC) as p:
    conv = p.map(convert_bsv, rb)

  # === Declare activity.
  wrote_something = (sum([0 if n < 0 else 1 for n in conv]) > 0)
  if wrote_something:
    print(activity)

    end_time = datetime.now().isoformat(timespec='minutes')
    print("## --- GRAPH ex:graph/activities --- ##")
    print('ex:resources/conversion_%s prov:endedAtTime' % DT_NOW)
    print('    "%s"^^xsd:dateTime.' % end_time)
    print("## --- UPDATE --- ##")

  
