# Lit du turtle-rdf sur l'entrée standard et en fait une (des) clause(s)
# SPARQL UPDATE pour ajouter les données dans le triplestore.
#
# AU PASSAGE :
# - Remplace les expressions prefix:element par l'URI complète,
#   pour permettre dles écritures type ex:truc/machin/chose (ce que le SPARQL
#   de Jena-Fuseki refuse - il génère une erreur en rencontrant le / )
# - Il envoie une requête INSERT DATA à chaque fois qu'il rencontre une ligne
#   qui matche "^## --- UPDATE --- ##$" (après un strip()), ce qui permet
#   d'ajouter des très très gros fichiers, en les découpant.
# - Lit les lignes qui matchent "^## --- GRAPH ex:graphe --- ##$" pour générer
#   une clause GRAPH ex:graphe { … } avec les triplets qui suivent jusqu'à
#   l'occurence suivante ou jusqu'au UPDATE suivant.
#   À noter que INSER DATA { GRAPH G1 {…} GRAPH G2 {…}} fonctionne.
#   À noter aussi que ex:graphe est aussi remplacé par l'URI (tant qu'à faire).
#
#
# Exemple :
#   \ls tmp/BSV/* | python vespa2ttl.py | python doSparqlUpdate.py
import sys
import io
import os
import re

import requests
from etl_functions import sparql_update, load_prefixes
from config import *

# +--------------------------------------------------------------+
# |                          CONFIG                              |
# +--------------------------------------------------------------+
VERBOSE_OUTPUT = DEBUG

SPARQL_ENDPOINT = SPARQL_SERVER + BSV_DATASET + '/update'


# +--------------------------------------------------------------+
# |                           main                               |
# +--------------------------------------------------------------+
find_prefix = re.compile(r'@?prefix\s+(\S+):\s*<([\S]+)>\s*\.?', flags=re.I)
find_prefixed = re.compile(r'(^|\.|\{|,|;|\s)\s*([^<"'+'\''+'\s][^\s:\^]*):([^>\s:,;\}\.]+)(\s|,|\.|\}|;|$)')
find_break = re.compile("^## --- UPDATE --- ##$")
find_graph = re.compile(r"^## --- GRAPH\s+(\S+)\s+--- ##$")
find_debug = re.compile(r"^\[DEBUG\].*$")

prefixes = load_prefixes(PREFIXES)

if VERBOSE_OUTPUT:
    print("Préfixes :")
    for k in prefixes:
        print(" %s:   <%s>" % (k,prefixes[k]))
    print()

ttl = ""
default_graph = True
line_nb = 0
for line in sys.stdin:
  if find_debug.search(line) is None:
    line_nb += 1
    line = line.strip()
    li = line.strip()
    m = find_prefix.search(li)
    while m is not None:
        if prefixes.get(m[1]) is None:
            prefixes[m[1]] = m[2]
            if VERBOSE_OUTPUT:
                print('Ajout du préfixe %s: <%s>.' % (m[1], m[2]))
        elif prefixes[m[1]] != m[2]:
            sys.stderr.write("\nATTENTION: préfixe %s: défini 2× :\n" % m[1])
            sys.stderr.write("    <%s> et <%s>.\n" % (prefixes[m[1]], m[2]))
            sys.stderr.write("    <%s> sera utilisé.\n\n" % prefixes[m[1]])
        li = li[:m.start()]+li[m.end():]
        m = find_prefix.search(li)

    right_li = li+' '
    li = ""
    m = find_prefixed.search(right_li)
    while m is not None:
        if prefixes.get(m[2]) is None:
            sys.stderr.write("\nATTENTION : préfixe %s: non trouvé (ligne %d) :\n" % (
                m[2], line_nb))
            sys.stderr.write("%s\n\n" % line)
            li = "%s%s" % (li,right_li[:m.end()])
        else:
            li = "%s%s%s<%s%s>%s" % (li,right_li[:m.start()],
                                    m[1],prefixes[m[2]],m[3],m[4])
        right_li = right_li[m.end():]
        m = find_prefixed.search(right_li)

    li = ("%s%s" % (li, right_li)).strip()
    m = find_graph.match(li)
    if m is not None:
        li = li[m.end():]
        g = m[1]
        if default_graph:
            if g.lower() != 'default':
                ttl = "%sGRAPH %s {\n" % (ttl, g)
                default_graph = False
        else:
            if g.lower() == 'default':
                ttl = "%s}\n" % ttl
                default_graph = True
            else:
                ttl = "%s} GRAPH %s {\n" % (ttl, g)
                default_graph = False

    m = find_break.match(li)
    if m is None:
        if len(li.strip()) > 0:
            ttl = "%s  %s\n" % (ttl, li)
    else:
        if len(ttl.strip()) > 0:
            if not default_graph:
                ttl = "%s}\n" % ttl
            query = ""
            for p in prefixes:
                query = "%sPREFIX %s: <%s>\n" % (query, p, prefixes[p])
            query = "%s\nINSERT DATA {\n%s}" % (query, ttl)
            if VERBOSE_OUTPUT:
                print("="*80)
                n = 1
                for l in query.split('\n'):
                    print("%6d %s" % (n,l))
                    n = n+1
                #print(query)
                print()
            if SPARQL_SERVER is not None:
                code,text = sparql_update(SPARQL_ENDPOINT, query)
                if code is not None:
                    sys.stderr.write("## SPARQL ERREUR %d : %s\n" % (code, text))
                elif VERBOSE_OUTPUT:
                    print("  -- Requête effectuée avec succès. --")
        ttl = ""
        default_graph = True
  else: # If [DEBUG] found, write it on standard output.
    print(line)



## Finished, have to send current request.
if len(ttl.strip()) > 0:
    if not default_graph:
        ttl = "%s}\n" % ttl
    query = ""
    for p in prefixes:
        query = "%sPREFIX %s: <%s>\n" % (query, p, prefixes[p])
    query = "%s\nINSERT DATA {\n%s}" % (query, ttl)
    if VERBOSE_OUTPUT:
        print("="*80)
        n = 1
        for l in query.split('\n'):
            print("%6d %s" % (n,l))
            n = n+1
        #print(query)
    if SPARQL_SERVER is not None:
        code,text = sparql_update(SPARQL_ENDPOINT, query)
        if code is not None:
            sys.stderr.write("## SPARQL ERREUR %d : %s\n" % (code, text))
        elif VERBOSE_OUTPUT:
            print("  -- Requête effectuée avec succès. --")
