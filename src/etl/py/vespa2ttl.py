import sys
import io
import os
import re

import requests
import json
import csv
from datetime import datetime, date, timedelta, timezone
from config import *
from etl_functions import *



# +--------------------------------------------------------------+
# |                          CONFIG                              |
# +--------------------------------------------------------------+

# Si not None, alors un fichier csv est chargé pour y chercher des dates
# de publication extraites manuellement.
#CSV_DATE_FILE = None
CSV_DATE_FILE = '/home/stef/Boulot/Devel/d2kab/src/etl/py/data/datesManuVespa.csv'
CSV_DATE_DELIMITER = ','
CSV_DATE_COLUMNS = ['date', 'bsv'] # Other than 'date' and 'bsv' will be ignored
CSV_GET_BSV = lambda s : os.path.splitext(os.path.basename(s))[0]
CSV_GET_XSD_DATE = lambda s : '-'.join([s.split('/')[x] for x in [2,1,0]])



# +--------------------------------------------------------------+
# |                        CONSTANTES                            |
# +--------------------------------------------------------------+
OLD_REGIONS = {
  '11':{'nom': 'Île-de-France', 'wd': 'Q13917'} ,
  '21':{'nom': 'Champagne-Ardenne', 'wd': 'Q14103'} ,
  '22':{'nom': 'Picardie', 'wd': 'Q13950'} ,
  '23':{'nom': 'Haute-Normandie', 'wd': 'Q16961'} ,
  '24':{'nom': 'Centre', 'wd': 'Q13947'} ,
  '25':{'nom': 'Basse-Normandie', 'wd': 'Q16954'} ,
  '26':{'nom': 'Bourgogne', 'wd': 'Q1173'} ,
  '31':{'nom': 'Nord-Pas-de-Calais', 'wd': 'Q16987'} ,
  '41':{'nom': 'Lorraine', 'wd': 'Q1137'} ,
  '42':{'nom': 'Alsace', 'wd': 'Q1142'} ,
  '43':{'nom': 'Franche-Comté', 'wd': 'Q16394'} ,
  '52':{'nom': 'Pays de la Loire', 'wd': 'Q16994'} ,
  '53':{'nom': 'Bretagne', 'wd': 'Q12130'} ,
  '54':{'nom': 'Poitou-Charentes', 'wd': 'Q17009'} ,
  '72':{'nom': 'Aquitaine', 'wd': 'Q1179'} ,
  '73':{'nom': 'Midi-Pyrénées', 'wd': 'Q16393'} ,
  '74':{'nom': 'Limousin', 'wd': 'Q1190'} ,
  '82':{'nom': 'Rhône-Alpes', 'wd': 'Q463'} ,
  '83':{'nom': 'Auvergne', 'wd': 'Q1152'} ,
  '91':{'nom': 'Languedoc-Roussillon', 'wd': 'Q17005'} ,
  '93':{'nom': 'Provence-Alpes-Côte d\'Azur', 'wd': 'Q15104'} ,
  '94':{'nom': 'Corse', 'wd': 'Q14112'} ,
  '01':{'nom': 'Guadeloupe', 'wd': 'Q17012'} ,
  '02':{'nom': 'Martinique', 'wd': 'Q17054'} ,
  '03':{'nom': 'Guyane', 'wd': 'Q3769'} ,
  '04':{'nom': 'La Réunion', 'wd': 'Q17070'} ,
  '06':{'nom': 'Mayotte', 'wd': 'Q17063'} ,
}

# +--------------------------------------------------------------+
# |                           main                               |
# +--------------------------------------------------------------+
IS_TEST_CORPUS = False
DEST_PATH = None
if (len(sys.argv) >= 1):
  if "--help" in sys.argv[1:]:
      print("Usage : python %s [options] [<file1> [<file2> …]]" % sys.argv[0])
      print("  If no filename is given, it'll get them from standard input")
      print("  (so you can pipe the result of a find command for example).")
      print('Options :')
      print("  --corpus-test : The given files are included in test corpus.")
      print("  --copy_bsv=/path/to/dest : Do a copy of the pdf files,")
      print("                  organized the same way as their URL into")
      print("                  dest directory.")
      print("  --debug : Print step by step process.")
      print("            Result should not be piped to doSparqlUpdate.py.")
      print("")
      sys.exit(0)
  IS_TEST_CORPUS = ("--corpus_test" in sys.argv[1:])
  if IS_TEST_CORPUS :
    sys.argv.remove("--corpus_test")
  DEBUG = ("--debug" in sys.argv[1:])
  if DEBUG :
    sys.argv.remove("--debug")
  for arg in sys.argv:
    if arg.startswith("--copy_bsv"):
      if not arg.startswith("--copy_bsv="):
        print("ERROR : --copy_bsv option's syntax is --copy_bsv=/path/to/dest")
        sys.exit(-1)
      DEST_PATH = '='.join(arg.split('=')[1:])
      sys.argv.remove(arg)
      break

if DEST_URL[-1] != '/':
  DEST_URL += '/'
if DEST_PATH == 'None': DEST_PATH = None
if DEST_PATH is not None:
  DEST_PATH = os.path.realpath(os.path.expanduser(DEST_PATH)) + '/'

if DEBUG:
  print("[DEBUG] Parameters :")
  print("[DEBUG]   DEBUG is ON")
  print("[DEBUG]   DEST_PATH = %s" % DEST_PATH)
  print("[DEBUG]   DEST_URL = %s" % DEST_URL)
  if IS_TEST_CORPUS:
    print("[DEBUG]   Is a test corpus.")
  else:
    print("[DEBUG]   Is not a test corpus.")

dates_manu = None
if CSV_DATE_FILE is not None:
    dates_manu = {}
    with open(CSV_DATE_FILE, newline='') as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=CSV_DATE_COLUMNS,
                delimiter=CSV_DATE_DELIMITER)
        for row in reader:
            dates_manu[CSV_GET_BSV(row['bsv'])] = row['date']

prefixes = load_prefixes(PREFIXES)
if prefixes is None:
    ex = EX_PREFIX
else:
    ex = prefixes['ex']

#organisation, conversion = get_current_acivity_names()
#print(create_current_activity())

if (len(sys.argv) == 1):
    inp = sys.stdin
else:
    ch = "\n".join(sys.argv[1:])
    inp = io.StringIO(ch)

BSV_written = 0
for line in inp:
    if line[-1] == '\n':
        line = line[:-1]
    if line[-1] == '*': # find command adds ugly trailing '*'…
        line = line[:-1]
    bsv_name = os.path.splitext(os.path.basename(line))[0]
    if DEBUG:
        print("[DEBUG] ===== BSV_NAME : %s" % bsv_name)

    sel_bsv = sparql_query(VESPA_ENDPOINT+'/query',
"""SELECT ?bsv ?reg ?dt ?dq ?desc
WHERE {
  ?bsv a <http://ontology.irstea.fr/bsv/ontology/Bulletin> ;
    <http://purl.org/dc/terms/spatial> ?reg ;
    <http://purl.org/dc/terms/date> ?dt ;
    <http://ontology.irstea.fr/bsv/ontology/dateExtractionQuality> ?dq ;
    <http://purl.org/dc/terms/description> ?desc
  BIND (str(?bsv) AS ?b) FILTER (STRENDS(?b, "%s"))
}""" % bsv_name)

    if sel_bsv is not None:
        if (len(sel_bsv) == 0):
            sys.stderr.write("=ERROR=====> %s :\n" % bsv_name)
            sys.stderr.write("  BSV non trouvé.\n")
        else:
            rb = sel_bsv[0]
            date = rb['dt']['value'].split('T')[0]
            dateq = rb['dq']['value']
            desc = re.sub(r' +', ' ', rb['desc']['value'].replace('\n', ' ')).strip()
            if re.match('.*[?.!]$', desc): # Remove final ponctuation.
              desc = desc[:-1]
            region = OLD_REGIONS[rb['reg']['value'].split('/')[-1]]['wd']
            manu = False

            if dates_manu.get(bsv_name) is not None:
                dt = None
                try:
                    dt = CSV_GET_XSD_DATE(dates_manu[bsv_name])
                except Exception as e:
                    pass
                if dt is not None:
                    date = dt
                    dateq = '100'
                    manu = True

            if DEBUG:
                if manu:
                    print("[DEBUG]   date : %s, relevée manuellement." % date)
                else:
                    print("[DEBUG]   date : %s" % date)
                print("[DEBUG]   qual : %s" % dateq)
                print("[DEBUG]   reg  : %s" % region)
                print("[DEBUG]   desc : %s" % desc)


            d2kab_basename = get_d2kab_basename(line)
            d2kab_bsvname = get_d2kab_bsvname(line, region, date)
            url_pdf = "%spdf/%s.pdf" % (DEST_URL, d2kab_bsvname)
            url_html = "%shtml/%s.html" % (DEST_URL, d2kab_bsvname)

            if DEST_PATH is not None:
                dest = "%spdf/%s.pdf" % (DEST_PATH, d2kab_bsvname)
                if DEBUG:
                    print("[DEBUG]   copy to %s" % dest)
                d = file_copy(line, dest)
                if d != dest:
                    url_pdf = "%s%s" % (DEST_URL, d[len(DEST_PATH):])
                    num = re.findall(r'_[0-9]+\.pdf', d)[-1][1:4]
                    url_html = "%shtml/%s_%s.html" % (DEST_URL, d2kab_bsvname, num)

            if TEST_URL:
                exists_pdf = url_exists(url_pdf)
                exists_html = url_exists(url_html)
                if DEBUG:
                    print("[DEBUG]   %s exists : %s" % (url_pdf, exists_pdf))
                    print("[DEBUG]   %s exists : %s" % (url_html, exists_html))
            else:
                exists_pdf = True
                exists_html = True
                if DEBUG:
                    print("[DEBUG]   url_pdf : %s" % (url_pdf))
                    print("[DEBUG]   url_html : %s" % (url_html))

            # ===============================
            # ===== Now write turte-rdf =====
            # ===============================
            bsv_uri = "%sresources/%s" % (ex, d2kab_basename)
            ttl = "## --- GRAPH ex:graph/corpus --- ##\n"
            ttl += "ex:resources/corpusVespa prov:hadMember <%s> .\n" % bsv_uri
            if IS_TEST_CORPUS:
                ttl += "ex:resources/corpusTestVespa prov:hadMember <%s> .\n" % bsv_uri

            ttl += "## --- GRAPH ex:graph/activities --- ##\n"
            ttl += 'ex:resources/collecte_manuelle'
            ttl += ' prov:generated <%s_pdf> .\n' % bsv_uri

            ttl += "## --- GRAPH ex:graph/bsv --- ##\n"
            ttl += "<%s> a d2kab:Bulletin, prov:Entity ;\n" % bsv_uri
            ttl += '  dct:description "%s."@fr ;\n' % desc
            ttl += '  prov:wasGeneratedBy ex:resources/collecte_manuelle ;\n'
            ttl += '  dct:spatial wd:%s ;\n' % region
            ttl += '  dct:date "%s"^^xsd:date ;\n' % date
            ttl += '  d2kab:dateExtractionQuality "%s %%"^^cdt:dimensionless ;\n' % dateq
            ttl += '  dul:isRealizedBy <%s_pdf> .\n' % bsv_uri
            ttl += '<%s_pdf> a prov:Entity, dct:Text ;\n' % bsv_uri
            if TEST_URL and not exists_pdf:
                ttl += "  # WARNING: URL doesn't exist (yet) :\n"
            ttl += '  schema:url <%s> ;\n' % url_pdf
            ttl += '  prov:wasGeneratedBy ex:resources/collecte_manuelle ;\n'
            ttl += '  rdfs:comment "Entité qui représente une copie, '
            ttl += 'stockée sur le serveur TSCF, du %s."@fr ;\n' % desc
            ttl += '  dce:language "fr-FR" ;\n'
            ttl += '  dce:format "application/pdf" ;\n'
            ttl += '  oa:textDirection oa:ltr .\n'

            print(ttl)

            BSV_written += 1
            if (BSV_written % UPDATE_EVERY_N_BSV) == 0:
                print("## --- UPDATE --- ##")

