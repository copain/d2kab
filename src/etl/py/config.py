########################################################################
# IMPORTANT :
# Parce que c'est possible, on utilise ce fichier de config pour
# les programmes python que pour les scripts bash.
# Pour cette raison, on garde une syntaxe compatible avec les deux,
# à savoir une affectation dépourvue d'espaces entre la variable,
# le symbole = et la valeur.
########################################################################

# Si DEBUG = True, la sortie est verbeuse et non exploitable (ça n'est pas
# du code ttl "propre"), mais c'est utile pendant le développement.
## OBSOLETE pour de nombreux programmes : remplacé par l'argument --debug
DEBUG=False

# Si True, alors on teste que le BSV est bien téléchargeable à l'URL
# qu'on aura générée
TEST_URL=False

# Le fichier contenant les préfixes. Voir commentaire de EX_PREFIX
PREFIXES='/home/stef/Boulot/Devel/d2kab/src/etl/ttl/prefixes.ttl'

# La valeur du préfixe ex: dans prefixes.ttl 
# N'est utilisé par doSparqlUpdate que si PREFIXES n'a pas pu être chargé.
# Est utilisé aussi par update_jena-fuseki.sh.
# Ceci est nécessaire du fait de la présence de points dans les noms de fichiers
# des BSV : ex:resources/BSV_du_18.03.2018 est coupé à ex:resources/BSV_du_18
# en turtle-rdf. Et du coup ça génère une erreur. Donc on va mettre les URI
# des BSV en <..../resources/BSV_blabla> pour éviter les erreurs,
# en utilisant la valeur de EX_PREFIX.
EX_PREFIX='http://ontology.inrae.fr/bsv/'

# La base de l'URL à laquelle les réalisations du BSV pourront être téléchargées.
# Non utilisé par convertToHtml (qui récupère l'info dans le dataset)
DEST_URL='http://ontology.inrae.fr/bsv/files/'

## Si SPARQL_SERVER est None, 
## alors doSparqlUpdate.py affiche les requêtes mais ne les envoie pas.
#SPARQL_SERVER=None

# Start local server with : ~/opt/share/jena-fuseki/fuseki-server --mem /bsv
SPARQL_SERVER='http://localhost:3030'
BSV_DATASET='/bsv'

#SPARQL_SERVER='http://ontology.inrae.fr:3030'
#BSV_DATASET='/bsv_test'

# Pour faire une copie du dataset collect :
# s-get http://ontology.irstea.fr:3030/bsv_dwnl/ default > /home/stef/Boulot/Data/BSV/BKP_collect.ttl
COLLECT_ENDPOINT='http://localhost:3030/dwnl'

VESPA_ENDPOINT='http://localhost:3030/vespa'

# Écrit une ligne de commentaires tous les UPDATE_EVERY_N_BSV destinée
# à provoquer une exécution de requête UPDATE.
# La ligne de commentaires est : ## --- UPDATE --- ##
UPDATE_EVERY_N_BSV = 20
