# TODO

## Renommer les fichiers des BSV avant l'annotation.

Certains caractères composant les noms de fichiers tels qu'ils sont
téléchargés sont à éviter dans les URI ou dans les noms de fichiers de
certains systèmes de fichiers. Pour éviter ceci, la fonction *slugify*
(dans  *etl_functions.py*) est utilisée.

Toutefois, lorsqu'AlvisNLP parcourt les fichiers d'un répertoire,
il restitue leur nom tel quel. Rien n'assure que ces noms
ont été convenablement «slugifiés» avant l'annotation.

Or le nom de fichier renvoyé par AlvisNLP est utilisé pour
écrire l'URI de la valeur *oa:hasSource* pour les
*oa:RessourceSelection*. Or l'URI des *prov:Entity* qui correspondent
sont composés des noms des fichiers de BSV *slugifiés*.

Par conséquent, il faut s'assurer de l'uniformité entre les noms
de fichiers et leur version *slugifiée* avant l'exécution d'AlvisNLP.



