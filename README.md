# D2KAB

version  Inrae du projet D2KAB dédié à la tache 4.3 
sur la conception du navigateur augmenté des Bulletins de Santé du Végétal (BSV).
Ce répertoire est privé.

## SKOS
Le répertoire skos contient les modeles skos définis pour déclarer les référentiels.
L'echelle générique BBCH a été défini comme une spécialisation du modèle skos et est en cours de développement.
Dans ce répertoire vous trouverez:

* ppdo.owl : ce fichier est le module de base contenant les classes et 
les propriétés pour définir un référentiel de stade de développement fondé sur la monographie BBCH. 
Ce fichier est au format owl/xml et a été généré avec protégé.
* grapevine_scales.owl: ce fichier est la premiere version du référentiel de IFV associé à BBCH. Il comprend une modelisation RDF de 5 échelles de stades de développement de la vigne.
Ce fichier est au format owl/xml et a été généré avec Protégé. 
* stadePhenologiqueRessources: ce repertoire contient les ressources utilisées pour construire ppdo.owl et grapevine_scales.owl. 
Les versions françaises, anglaises de la monographie BBCH. Les fichiers excels stockant les informations sur les stades. 
Les regles de transformations des fichiers excels pour générer automatiquements les classes et instances. 
A noter que le plug in Cellfie de protégé a été utilisé pour générer automatiquement les classes et instances dans ppdo.owl et grapevine_scales.owl.

l'algorithme de construction de ces fichiers est disponible sur le wiki