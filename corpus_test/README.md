# Constitution de corpus de test

Les corpus de test sont extraits de l'ensemble des BSV collectés
dans le cadre des projets VESPA et D2KAB, entre 2009 et aujourd'hui.
La collecte s'effectue sur l'ensemble des régions de france métropolitaine
ainsi que sur Mayotte et la Guadeloupe.


### Corpus de test VESPA

Corpus de test vespa: un sous ensemble du corpus vespa composé de 500 BSV dont les index ont été construits manuellement. Ce corpus de test a été utilisé pour vérifier la qualité de processus d'indexation automatique utilisé pour indexer l'ensemble du corpus vespa.

Le développement de l'indexation dans le cadre du projet Vespa a donné lieu à une extraction manuelle de date pour 500 bulletins de santé du végétal, parmi les 19448 BSV collectés dans le cadre de ce projet.

Les régions et types de cultures ont été manuellement extraits lors des collectes de ces BSV.

On dispose donc d'un corpus permettant de tester l'extraction automatique de dates. Ces bulletins ayant été collectés de 2009 à 2015, les zones géographiques correspondent au découpage régional avant la réforme du 1er janvier 2016.

Année | Nombre de BSV
--- | ---:
2009 | 23
2010 | 59
2011 | 88
2012 | 98
2013 | 111
2014 | 93
Non datés | 28

En ce qui concerne les types de culture, il s'agit de rubriques indiquées dans les pages web de téléchargement (Grandes cultures, Viticulture, Jardins, ...).
Chaque rubrique a été associée manuellement à un ensemble de concepts issu du thésaurus french crop usage. À noter que les rubriques varient d'une région à l'autre et qu'il n'y a pas de liste harmonisée des types de cultures. C'est pour cette raison que nous avons créé le thésaurus french crop usage.

*Vespa corpus gathers 500 PHBs collected in the whole French territory between 2009 and 2015 and manually indexed. A PHB is indexed by its crop categories and its location, that is to say, its French administrative unit. The publication date was manually extracted.*

### Corpus de test D2KAB

Dans le cadre du projet D2KAB, un corpus de 230 BSV a été constitué manuellement. Ce corpus visant à extraire des informations sur la base de reconnaissance de vocabulaire, il porte sur des bulletins traitant de maraîchage, de viticulture et de grandes cultures uniquement.

Il ne contient que des BSV (il ne contient pas de notes nationales sur un ravageur, une adventice, ...). De fait, tous les bulletins qu'il contient ont une date de publication précise.

**Répartition :**

Année | Nombre de BSV
--- | ---:
2015 | 2
2018 | 17
2019 | 211

La sélection manuelle a porté sur la couverture de l'ensemble du territoire français. Toutes les regions de france métropolitaine sont couvertes.

*D2KAB corpus is composed of 230 PHBs collected in 2019. Those PHBs were manually selected to cover the whole French territory and to represent 3 crop categories: arable crops, vegetables and grapevines. Thus the crop categories and the French administrative unit are already identified. The publication date was automatically extracted.*

### Corpus de test aléa

Le corpus de test D2KAB étant relativement homogène, l'extension du processus d'indexation à l'ensemble des BSV collectés automatiquement nécessite un corpus plus aléatoire. 150 autres BSV ont donc été tirés aléatoirement parmi l'ensemble des BSV collectés automatiquement jusqu'à ce jour (novembre 2020).

De par son caractère aléatoire, 8 documents n'ont pas de date précise, 10 ont une zone géographique inter-régionale voire nationale, et 1 bulletin n'a pas de type de culture mentionné dans son contenu.

**Répartition :**

Année | Nombre de BSV
--- | ---:
2011 | 3
2012 | 2
2013 | 5
2014 | 1
2015 | 13
2016 | 10
2017 | 26
2018 | 25
2019 | 31
2020 | 26
Non datés | 8

*Alea corpus is composed of 150 PHBs randomly selected from the whole corpus. That is to say the publication date may vary from 2009 to 2020. No information are extracted or provided from those PHBs.*
