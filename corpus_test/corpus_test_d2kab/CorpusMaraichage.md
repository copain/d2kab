# Corpus de test D2KAB

Ce corpus de bulletins de santé du végétal est constitué
d'une sélection de bulletins sur des cultures maraîchères. Ces bulletins ont été
choisis sur 2019 pour couvrir le plus largement possible les différentes
éditions disponibles.

Il contient 59 bulletins de santé du végétal.

## Maraichage
### Auvergne Rhône-Alpes
##### Allium et pomme de terre
- BSV_legumes_allium_pomme_de_terre_AURA_2019-12_cle449f16 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019-12_cle449f16.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019-12_cle449f16.html)
- BSV_legumes_allium_pomme_de_terre_AURA_2019-14_cle48f73b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019-14_cle48f73b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019-14_cle48f73b.html)
- BSV_legumes_allium_pomme_de_terre_AURA_2019_-2_cle45e5c6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019_-2_cle45e5c6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019_-2_cle45e5c6.html)
- BSV_legumes_allium_pomme_de_terre_AURA_2019-8_cle03ef71 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019-8_cle03ef71.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18338206/2019/BSV_legumes_allium_pomme_de_terre_AURA_2019-8_cle03ef71.html)

### Bourgogne Franche-Comté
##### Légumes
- BSV_Legumes_n_08_du_17_07_19_cle838d16 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18578267/2019/BSV_Legumes_n_08_du_17_07_19_cle838d16.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18578267/2019/BSV_Legumes_n_08_du_17_07_19_cle838d16.html)


### Bretagne
##### Légumes industrie
- BSV_Legumes_industries_2019_No20_cle06a888 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_Legumes_industries_2019_No20_cle06a888.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_Legumes_industries_2019_No20_cle06a888.html)

##### Pomme de terre
- BSV_No8_pomme_de_terre_17_mai_2019_cle8f91d1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_No8_pomme_de_terre_17_mai_2019_cle8f91d1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_No8_pomme_de_terre_17_mai_2019_cle8f91d1.html)

##### Légumes frais
- BSV_no14_legumes_05_juillet_2019_cle0e639d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no14_legumes_05_juillet_2019_cle0e639d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no14_legumes_05_juillet_2019_cle0e639d.html)
- BSV_no15_legumes_12_juillet_2019_cle032af3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no15_legumes_12_juillet_2019_cle032af3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no15_legumes_12_juillet_2019_cle032af3.html)
- BSV_no19_legumes_9_aout_2019_cle82c98c [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q12130/2019/BSV_no19_legumes_9_aout_2019_cle82c98c.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q12130/2019/BSV_no19_legumes_9_aout_2019_cle82c98c.html)


### Centre - Val de Loire
- BSV_legumes_02_du_20-03-19_cle455343 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_legumes_02_du_20-03-19_cle455343.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_legumes_02_du_20-03-19_cle455343.html)
- BSV_legumes_16_du_24-07-19_cle4f7928 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_legumes_16_du_24-07-19_cle4f7928.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_legumes_16_du_24-07-19_cle4f7928.html)
- BSV_legumes_22_du_11-09-19__cle028fcc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_legumes_22_du_11-09-19__cle028fcc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_legumes_22_du_11-09-19__cle028fcc.html)
- BSV_legumes_27_du_06-11-19__cle059f89 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13947/2019/BSV_legumes_27_du_06-11-19__cle059f89.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13947/2019/BSV_legumes_27_du_06-11-19__cle059f89.html)



### Corse
- BSV_Corse_Maraichage_3_8juillet_2019_cle4de2c4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2019/BSV_Corse_Maraichage_3_8juillet_2019_cle4de2c4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2019/BSV_Corse_Maraichage_3_8juillet_2019_cle4de2c4.html)
- BSV_Corse_Maraichage_4_7aout_2019_cle83ed26 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2019/BSV_Corse_Maraichage_4_7aout_2019_cle83ed26.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2019/BSV_Corse_Maraichage_4_7aout_2019_cle83ed26.html)
- BSV_Corse_Maraichage_5_3sept_2019_cle8b6a1e [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q14112/2019/BSV_Corse_Maraichage_5_3sept_2019_cle8b6a1e.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q14112/2019/BSV_Corse_Maraichage_5_3sept_2019_cle8b6a1e.html)


### Grand Est
##### Légumes
- 20190502_BSV_legumes_cle079711 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190502_BSV_legumes_cle079711.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190502_BSV_legumes_cle079711.html)
- 20190731_BSV_legumes_cle01d6c6 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190731_BSV_legumes_cle01d6c6.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190731_BSV_legumes_cle01d6c6.html)
- 20190828_BSV_legumes_cle0ede1d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20190828_BSV_legumes_cle0ede1d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20190828_BSV_legumes_cle0ede1d.html)
- 20191114_BSV_legumes_cle021d18 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677983/2019/20191114_BSV_legumes_cle021d18.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677983/2019/20191114_BSV_legumes_cle021d18.html)


### Hauts de France
- BSV_Legumes_102019_cle8be56a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_102019_cle8be56a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_102019_cle8be56a.html)
- BSV_Legumes_132019_cle871f9a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_132019_cle871f9a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_132019_cle871f9a.html)
- BSV_Legumes_212019_cle835ca3 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_212019_cle835ca3.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_212019_cle835ca3.html)
- BSV_Legumes_262019_cle8bd5e8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_262019_cle8bd5e8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_262019_cle8bd5e8.html)
- BSV_Legumes_322019_2__cle44e13b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677767/2019/BSV_Legumes_322019_2__cle44e13b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677767/2019/BSV_Legumes_322019_2__cle44e13b.html)


### Île-de-France
- BSV_MARAICHAGE_2019_02_cle0c5c84 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_MARAICHAGE_2019_02_cle0c5c84.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_MARAICHAGE_2019_02_cle0c5c84.html)
- BSV_MARAICHAGE_2019_15_cle04166a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_MARAICHAGE_2019_15_cle04166a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_MARAICHAGE_2019_15_cle04166a.html)
- BSV_MARAICHAGE_2019_23_cle0745ab [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_MARAICHAGE_2019_23_cle0745ab.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_MARAICHAGE_2019_23_cle0745ab.html)
- BSV_MARAICHAGE_2019_32_cle0b4427 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q13917/2019/BSV_MARAICHAGE_2019_32_cle0b4427.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q13917/2019/BSV_MARAICHAGE_2019_32_cle0b4427.html)

### Normandie
##### Cultures légumières
- 2019_BSV_Normandie_leg_22_sem35_cle43a9ca [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18677875/2019/2019_BSV_Normandie_leg_22_sem35_cle43a9ca.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18677875/2019/2019_BSV_Normandie_leg_22_sem35_cle43a9ca.html)

### Nouvelle Aquitaine
- BSV_MARAICHAGE_Nord_NA_02_20190507_cle85ab9d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_02_20190507_cle85ab9d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_02_20190507_cle85ab9d.html)
- BSV_MARAICHAGE_Nord_NA_16__20190822_cle04e172 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_16__20190822_cle04e172.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_16__20190822_cle04e172.html)
- BSV_MARAICHAGE_Nord_NA_20__20190919_cle0d91fc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_20__20190919_cle0d91fc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_20__20190919_cle0d91fc.html)
- BSV_MARAICHAGE_Nord_NA_26_20191113_cle877a14 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_26_20191113_cle877a14.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_MARAICHAGE_Nord_NA_26_20191113_cle877a14.html)
- BSV_NA_MARAICHAGE_Sud_NA_02_20190325_cle46958b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_MARAICHAGE_Sud_NA_02_20190325_cle46958b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_MARAICHAGE_Sud_NA_02_20190325_cle46958b.html)
- BSV_NA_MARAICHAGE_Sud_NA_08_21092019_cle45e738 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_MARAICHAGE_Sud_NA_08_21092019_cle45e738.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_MARAICHAGE_Sud_NA_08_21092019_cle45e738.html)
- B_V_NA_MARAICHAGE_Sud_NA_09_20191028_cle4d1138 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/B_V_NA_MARAICHAGE_Sud_NA_09_20191028_cle4d1138.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/B_V_NA_MARAICHAGE_Sud_NA_09_20191028_cle4d1138.html)

##### Légumes de plein champ et d'industrie
- BSV_NA_LEGUMES_PC__INDUSTRIE_08_20190704_cle039911 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678082/2019/BSV_NA_LEGUMES_PC__INDUSTRIE_08_20190704_cle039911.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678082/2019/BSV_NA_LEGUMES_PC__INDUSTRIE_08_20190704_cle039911.html)


### Occitanie
- bsv_maraichage_lr_n02_23012019_cle055ce8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_lr_n02_23012019_cle055ce8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_lr_n02_23012019_cle055ce8.html)
- bsv_maraichage_lr_n08_17042019_cle0913a2 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_lr_n08_17042019_cle0913a2.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_lr_n08_17042019_cle0913a2.html)
- bsv_maraichage_lr_n13_26-06-2019_cle03aab1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_lr_n13_26-06-2019_cle03aab1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_lr_n13_26-06-2019_cle03aab1.html)
- bsv_maraichage_lr_n19_16102019_cle08165a [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_lr_n19_16102019_cle08165a.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_lr_n19_16102019_cle08165a.html)
- bsv_maraichage_lr_n22_11122019_cle073bba [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_lr_n22_11122019_cle073bba.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_lr_n22_11122019_cle073bba.html)
- bsv_maraichage_n04_10052019_cle0fbc6b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_n04_10052019_cle0fbc6b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_n04_10052019_cle0fbc6b.html)
- bsv_maraichage_n14_19082019_cle01948b [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_n14_19082019_cle01948b.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_n14_19082019_cle01948b.html)
- bsv_maraichage_n19_10102019_cle038cd4 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2019/bsv_maraichage_n19_10102019_cle038cd4.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2019/bsv_maraichage_n19_10102019_cle038cd4.html)
- bsv_maraichage_n15_23082018_cle0775f5 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_maraichage_n15_23082018_cle0775f5.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_maraichage_n15_23082018_cle0775f5.html)
- bsv_maraichage_no13_du_18_juillet_2018-2_cle0745d8 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_maraichage_no13_du_18_juillet_2018-2_cle0745d8.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_maraichage_no13_du_18_juillet_2018-2_cle0745d8.html)
- bsv_maraichage_no17_du_24_octobre_2018_cle82d6cc [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q18678265/2018/bsv_maraichage_no17_du_24_octobre_2018_cle82d6cc.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q18678265/2018/bsv_maraichage_no17_du_24_octobre_2018_cle82d6cc.html)


### Pays de la Loire
- 20180802_bsvmaraichage_19_cle04c2cf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/20180802_bsvmaraichage_19_cle04c2cf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/20180802_bsvmaraichage_19_cle04c2cf.html)
- 20180920_bsvmaraichage_cle0649bf [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/20180920_bsvmaraichage_cle0649bf.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/20180920_bsvmaraichage_cle0649bf.html)
- 20181011_bsvmaraichage_27_cle09c363 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2018/20181011_bsvmaraichage_27_cle09c363.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2018/20181011_bsvmaraichage_27_cle09c363.html)
- 20199062_bsvmaraichage_no27_cle051121-1 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q16994/2019/20199062_bsvmaraichage_no27_cle051121-1.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q16994/2019/20199062_bsvmaraichage_no27_cle051121-1.html)


### Provence-Alpes-Côte d'Azur
- bsv_maraichage_no17_du_09_Septembre_2019_cle0c1c44 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/bsv_maraichage_no17_du_09_Septembre_2019_cle0c1c44.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/bsv_maraichage_no17_du_09_Septembre_2019_cle0c1c44.html)
Culture Tomate, Aubergine,
Stade F7, 4 derniers bouquets, BBCH 89, R14 --> BBCH 89 R11--> BBCH 89, Récolte> 2 e  couronne (BBCH 89)
- bsv_maraichage_no18_du_20_Septembre_2019_cle01c961 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/bsv_maraichage_no18_du_20_Septembre_2019_cle01c961.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/bsv_maraichage_no18_du_20_Septembre_2019_cle01c961.html)
- bsv_maraichage_no2_du_25_Janvier_2019_cle0d6ef7 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/bsv_maraichage_no2_du_25_Janvier_2019_cle0d6ef7.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/bsv_maraichage_no2_du_25_Janvier_2019_cle0d6ef7.html)
- bsv_maraichage_no9_du_03_Mai_2019_cle842127 [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/bsv_maraichage_no9_du_03_Mai_2019_cle842127.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/bsv_maraichage_no9_du_03_Mai_2019_cle842127.html)

##### Tomate d’industrie
- BSV_Tomate_Industrie_2019_no4_cle87ed7d [pdf](http://ontology.inrae.fr/bsv/files/pdf/Q15104/2019/BSV_Tomate_Industrie_2019_no4_cle87ed7d.pdf) [html](http://ontology.inrae.fr/bsv/files/html/Q15104/2019/BSV_Tomate_Industrie_2019_no4_cle87ed7d.html)
