# Corpus de BSV

Ce corpus se découpe en trois sous-corpus ; un par cas d'usage:
- [Viticulture](https://gitlab.irstea.fr/copain/d2kab/-/blob/master/corpus_test/corpus_test_d2kab/CorpusViticulture.md),
- [Grande Culture](https://gitlab.irstea.fr/copain/d2kab/-/blob/master/corpus_test/corpus_test_d2kab/CorpusGC.md),
- [Maraichage](https://gitlab.irstea.fr/copain/d2kab/-/blob/master/corpus_test/corpus_test_d2kab/CorpusMaraichage.md).

Chaque sous-corpus est organisé par région administrative (nouvelle région).
